﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
//using Vectrosity;
//
///// <summary>
///// Will take care of the repeatibility experiment
///// </summary>
//public class Repeatibility : MonoBehaviour
//{
//
//    /// <summary>
//    /// (Field) The max of shots that will be performed
//    /// </summary>
//    private int m_MaxShots = 100;
//    /// <summary>
//    /// (Field) The array of all shots performed
//    /// </summary>
//    [SerializeField]
//    private Vector3[] m_ShotPositions;
//    /// <summary>
//    /// (Field) The current shot in the array
//    /// </summary>
//    [SerializeField]
//    private int m_CurrentShot;
//
//    /// <summary>
//    /// (Field) The Mean of the Shots
//    /// </summary>
//    [SerializeField]
//    private Vector3 m_Mean;
//
//    /// <summary>
//    /// (Field) Flag that controls if the test can be run
//    /// </summary>
//    [SerializeField]
//    private bool m_StopTest;
//
//    /// <summary>
//    /// (Field) The circle for the mean
//    /// </summary>
//    [SerializeField, Header("VectorLines")]
//    private VectorLine m_CircleMean;
//    /// <summary>
//    /// (Field) The circle for the max error
//    /// </summary>
//    private VectorLine m_MaxErrorCircle;
//    /// <summary>
//    /// (Field) The circle for the standard deviation
//    /// </summary>
//    private VectorLine m_StdDeviationCircle;
//    /// <summary>
//    /// (Field) The vector line for shots produced
//    /// </summary>
//    [SerializeField]
//    private VectorLine m_ShotsProduced;
//
//    /// <summary>
//    /// (Field) The MAx error distance between shots
//    /// </summary>
//    private float m_MaxErrorDistance;
//    private float m_TempErrorDistance;
//
//    /// <summary>
//    /// (Field) The array of distances
//    /// </summary>
//    [SerializeField]
//    private float[] m_ErrorDistances;
//    /// <summary>
//    /// (Field) The mean error distance
//    /// </summary>
//    [SerializeField]
//    private float m_MeanErrorDistance;
//
//    [Header("Standard Deviation Error Distance")]
//    float[] m_StandDeviationErrorDistance;
//    [SerializeField]
//    float m_VarianceErrorDistance;
//    [SerializeField]
//    float m_PopStandDeviationErrorDistance = 0f;
//
//
//    // Use this for initialization
//    void Start()
//    {
//        // We resize the array of shots to have MaxShots elements
//        Array.Resize(ref m_ShotPositions, m_MaxShots);
//        //m_ShotPositions = new List<Vector3>(m_MaxShots);
//
//        // We resize the array of error distances
//        Array.Resize(ref m_ErrorDistances, m_MaxShots);
//
//        // We create the vectorline for shots
//        m_ShotsProduced = new VectorLine("ShotsProduced", new List<Vector2>(), 2.0f);
//        m_ShotsProduced.lineType = LineType.Points;
//        // We make it child of the hud
//        m_ShotsProduced.rectTransform.parent = Toolbox.Instance.GameManager.HudController.ScoreText.transform;
//
//        // We create the circle
//        m_CircleMean = new VectorLine("CircleMean", new List<Vector2>(), 2.0f);
//        // We add 100 segments
//        for (int i = 0; i < m_MaxShots; i++)
//        {
//            m_CircleMean.points2.Add(Vector3.one);
//        }
//        m_CircleMean.lineType = LineType.Continuous;
//        m_CircleMean.color = Color.red;
//        // We make it child of the hud
//        m_CircleMean.rectTransform.parent = Toolbox.Instance.GameManager.HudController.ScoreText.transform;
//
//
//        // We create the circle
//        m_MaxErrorCircle = new VectorLine("MaxErrorCircle", new List<Vector2>(), 2.0f);
//        // We add 100 segments
//        for (int i = 0; i < m_MaxShots; i++)
//        {
//            m_MaxErrorCircle.points2.Add(Vector3.one);
//        }
//        m_MaxErrorCircle.lineType = LineType.Continuous;
//        // We make it child of the hud
//        m_MaxErrorCircle.rectTransform.parent = Toolbox.Instance.GameManager.HudController.ScoreText.transform;
//
//        // We create the circle
//        m_StdDeviationCircle = new VectorLine("StdDeviationCircle", new List<Vector2>(), 2.0f);
//        // We add 100 segments
//        for (int i = 0; i < m_MaxShots; i++)
//        {
//            m_StdDeviationCircle.points2.Add(Vector3.one);
//        }
//        m_StdDeviationCircle.lineType = LineType.Continuous;
//        m_StdDeviationCircle.color = Color.green;
//        // We make it child of the hud
//        m_StdDeviationCircle.rectTransform.parent = Toolbox.Instance.GameManager.HudController.ScoreText.transform;
//
//    }
//    
//    // This function is called when the object becomes enabled and active
//    public void OnEnable()
//    {
//        Initialize(true);
//    }
//
//    // This function is called when the behaviour becomes disabled or inactive
//    public void OnDisable()
//    {
//        Initialize(false);
//    }
//
//    /// <summary>
//    /// Initialize class variables to desire values
//    /// </summary>
//    private void Initialize(bool value)
//    {
//        // Vector lines
//        //m_CircleMean.drawTransform.GetComponent<VectorObject2D>().enabled = value;
//        //m_StdDeviationCircle.drawTransform.GetComponent<VectorObject2D>().enabled = value;
//        //m_MaxErrorCircle.drawTransform.GetComponent<VectorObject2D>().enabled = value;
//
//        // Statistics
//        m_MeanErrorDistance = 0f;
//        m_VarianceErrorDistance = 0f;
//        m_PopStandDeviationErrorDistance = 0f;
//
//        // Current shot
//        m_CurrentShot = 0;
//
//        // Flag
//        m_StopTest = !value;
//
//    }
//
//    /// <summary>
//    /// Adds a shot to the array os positions
//    /// </summary>
//    /// <param name="shotPos"></param>
//    public void AddShotToList(Vector3 shotPos)
//    {
//        if (!m_StopTest)
//        {
//            // If the counter is below the maxShots...
//            if (m_CurrentShot < m_MaxShots)
//            {
//                // We add the shot passed in to the array of shots
//                m_ShotPositions[m_CurrentShot] = shotPos;
//
//                // We calculate the distance from the origin to the last shot
//                m_TempErrorDistance = Vector3.Distance(m_ShotPositions[0], m_ShotPositions[m_CurrentShot]);
//
//                // We save that distance in the array of distances
//                m_ErrorDistances[m_CurrentShot] = m_TempErrorDistance;
//
//                // If that distance is greater than the previous max error distance...
//                if (m_TempErrorDistance > m_MaxErrorDistance)
//                {
//                    // We update the error distance with it
//                    m_MaxErrorDistance = m_TempErrorDistance;
//                }
//
//                // We draw the shot
//                DrawShotPoints();
//
//                // We draw the error circle
//                DrawSpreadErrorCircle();
//
//                // We increase the counter of shots
//                m_CurrentShot++;
//
//            }
//            // If the counter is equal or more the maxShots...
//            else if (m_CurrentShot >= m_MaxShots)
//            {
//                // ... We calculate the mean
//                CalculateShotPositionMean();
//                CalculateErrorDistanceMean();
//                CalculateStandradDeviationErrorDistance();
//
//                // We draw the mean circle
//                DrawMeanCircle();
//
//                // We draw the std deviation circle
//                DrawStdDeviationErrorCircle();
//
//                // We show the data on the HUD
//                PrintDataOnHUD();
//
//                // We stop the test to make sure that we not call this more 
//                m_StopTest = true;
//            }
//        }
//
//    }
//
//    /// <summary>
//    /// Calculates the Mean of the ShotsPositions
//    /// </summary>
//    private void CalculateShotPositionMean()
//    {
//        // We sum all the elements of the shotPositions list
//        for (int i = 0; i < m_MaxShots; i++)
//        {
//            m_Mean += m_ShotPositions[i];
//        }
//        // We divided by the total number of elements
//        m_Mean = m_Mean / m_MaxShots;
//    }
//
//    /// <summary>
//    /// Calculates the mean of the error distance
//    /// </summary>
//    private void CalculateErrorDistanceMean()
//    {
//        // We sum all the elemetns of the errorDistances
//        for (int i = 0; i < m_MaxShots; i++)
//        {
//            m_MeanErrorDistance += m_ErrorDistances[i];
//        }
//
//        // We divide by the total of the Error Distance
//        m_MeanErrorDistance = m_MeanErrorDistance / m_MaxShots;
//    }
//
//    /// <summary>
//    /// Draws the circle of the mean error distance
//    /// </summary>
//    private void DrawMeanCircle()
//    {
//        //m_CircleMean.MakeCircle(m_ShotPositions[0], Vector3.Distance(m_ShotPositions[0], m_Mean));
//        m_CircleMean.MakeCircle(m_ShotPositions[0], m_MeanErrorDistance);
//        m_CircleMean.Draw();
//        //m_CircleMean.MakeCircle(Toolbox.Instance.GameManager.InputController.ScreenPointerPos, 2f);
//    }
//
//    /// <summary>
//    /// Draws the Circle of the max error distance
//    /// </summary>
//    private void DrawSpreadErrorCircle()
//    {
//        m_MaxErrorCircle.MakeCircle(m_ShotPositions[0], m_MaxErrorDistance);
//        m_MaxErrorCircle.Draw();
//    }
//
//
//    /// <summary>
//    /// Draws the Circle of the std deviation error distance
//    /// </summary>
//    private void DrawStdDeviationErrorCircle()
//    {
//        m_StdDeviationCircle.MakeCircle(m_ShotPositions[0], m_PopStandDeviationErrorDistance);
//        m_StdDeviationCircle.Draw();
//    }
//
//    /// <summary>
//    /// Draws each of the points of the shots
//    /// </summary>
//    private void DrawShotPoints()
//    {
//        // We add the last shot performed
//        m_ShotsProduced.points2.Add(m_ShotPositions[m_CurrentShot]);
//        // We draw it
//        m_ShotsProduced.Draw();
//    }
//
//    /// <summary>
//    /// Calculates standard deviation of the shots
//    /// </summary>
//    private void CalculateStandradDeviationErrorDistance()
//    {
//        // We define the array of standard Deviation 
//        Array.Resize(ref m_StandDeviationErrorDistance, m_MaxShots);
//        m_VarianceErrorDistance = 0f;
//        m_PopStandDeviationErrorDistance = 0f;
//
//        // First, calculate the deviations of each data point from the mean, and square the result of each:
//        for (int i = 0; i < m_MaxShots; i++)
//        {
//            // Calculate deviations of each data point from the mean
//            m_StandDeviationErrorDistance[i] = m_ErrorDistances[i] - m_MeanErrorDistance;
//            // Square the result of each
//            m_StandDeviationErrorDistance[i] = m_StandDeviationErrorDistance[i] * m_StandDeviationErrorDistance[i];
//        }
//
//        // We calculate the VARIANCE (which is the mean of these values)
//        for (int i = 0; i < m_MaxShots; i++)
//        {
//            m_VarianceErrorDistance += m_StandDeviationErrorDistance[i];
//        }
//        m_VarianceErrorDistance = m_VarianceErrorDistance / m_MaxShots;
//
//        // We calculate the POPULATION STANDARD DEVIATION, which is equal to the square root of the variance
//        m_PopStandDeviationErrorDistance = Mathf.Sqrt(m_VarianceErrorDistance);
//    }
//
//    /// <summary>
//    /// Prints all the data on the HUD
//    /// </summary>
//    private void PrintDataOnHUD()
//    {
//        Toolbox.Instance.GameManager.HudController.UpdateStoryText("Mean: " + m_MeanErrorDistance.ToString() + "\nVariance: " + m_VarianceErrorDistance.ToString() + "\nStd Deviation: " + m_PopStandDeviationErrorDistance.ToString() + "\n");
//    }
//}
