﻿using UnityEngine;
using System.Collections.Generic;

public class Performance_Test : MonoBehaviour {

    /// <summary>
    /// (Field) The list of FPS each frame
    /// </summary>
    [SerializeField]
    private List<float> m_FPSList;
    /// <summary>
    /// (Field) The timer controller for the FPSList
    /// </summary>
    private TimerController m_FPSListTimer;
    /// <summary>
    /// (Field) The amount of seconds to run the test
    /// </summary>
    [SerializeField]
    private float m_TimeToRunTest;
    /// <summary>
    /// (Field) The final Average FPS to calculate
    /// </summary>
    [SerializeField]
    private float m_AverageFPS;
    /// <summary>
    /// (Field) Flag to control if the test can run
    /// </summary>
    [SerializeField]
    private bool m_StopTest;

	// Use this for initialization
	void Start () {
        // We assign the timer controller
        m_FPSListTimer = this.gameObject.AddComponent<TimerController>();
        // We set the label of the timer list
        m_FPSListTimer.ObjectLabel = "FPSTimerList";

        // We initialize the list
        m_FPSList = new List<float>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!m_StopTest)
        {
            // We wait X seconds...
            if (m_FPSListTimer.GenericCountDown(m_TimeToRunTest))
            {
                // We calculate the average
                CalculateFPSAverage();
                // We stop the test
                m_StopTest = true;
            }
            // Every second we wait...
            else
            {
                // We add one FPS to the list
                m_FPSList.Add(1f / Time.unscaledDeltaTime);
            }
        }
        
	}

    /// <summary>
    /// Calculates the average of FPS based on the list
    /// </summary>
    private void CalculateFPSAverage ()
    {
        // Reset average FPS
        m_AverageFPS = 0;
        // Go over the list of FPS
        for (int i = 0; i < m_FPSList.Count; i++)
        {
            m_AverageFPS += m_FPSList[i];
        }
        // We divide the total sum of elements by the number of elements
        m_AverageFPS = m_AverageFPS / m_FPSList.Count;
    }
}
