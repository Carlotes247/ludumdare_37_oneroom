﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PSMoveInputCtrl : MonoBehaviour {

    // List of Psmove controllers
    public List<PSMoveController> PSMoves;

    public enum PointerSensorEnum
    {
        Gyroscope,
        Fusion
    }
    public PointerSensorEnum m_PointerSensor;

    public Vector3 ScreenGyroPointer;

    public float m_GyroAmplifyCoeff;

    private Vector2 m_CenterScreen;

    public Vector3 m_GyroDeadZone;

    private Quaternion m_LastOrientation;
    private Quaternion m_NewOrientation;
    private float m_AngleDiff;


	// Use this for initialization
	void Start () {
        // We try to find all of the psmovecontrollers in the scene
        PSMoves = (GameObject.FindObjectsOfType<PSMoveController>()).ToList<PSMoveController>();

        // Calculate the center of the screen
        m_CenterScreen.x = Screen.width * 0.5f;
        m_CenterScreen.y = Screen.height * 0.5f;

        // Set the gyroPointer to the center
        ScreenGyroPointer = m_CenterScreen;

        PSMoves[0].OnButtonMovePressed += move_RecenterGyroPointer;

    }


    // Update is called once per frame
    void Update () {
        UpdateGyroPointer();
    }

    /// <summary>
    /// Updates the values of the gyro Pointer
    /// </summary>
    private void UpdateGyroPointer()
    {
        float auxGyroX = 0;
        float auxGyroY = 0;
        //Gyro.z += PSMoves[0].Gyroscope.y * amplifyCoeff;

        switch (m_PointerSensor)
        {
            case PointerSensorEnum.Gyroscope:
                auxGyroX = PSMoves[0].Gyroscope.z;
                auxGyroY = PSMoves[0].Gyroscope.x;
                break;
            case PointerSensorEnum.Fusion:
                m_NewOrientation = PSMoves[0].Orientation;
                auxGyroX = m_NewOrientation.y - m_LastOrientation.y;
                auxGyroY = m_NewOrientation.x - m_LastOrientation.x;
                break;
            default:
                break;
        }

        // Deadzone X axis
        if (Mathf.Abs(auxGyroX) < m_GyroDeadZone.x)
        {
            auxGyroX = 0f;
        }
        // Deadzone Y axis
        if (Mathf.Abs(auxGyroY) < m_GyroDeadZone.y)
        {
            auxGyroY = 0f;
        }

        ScreenGyroPointer.x -= auxGyroX * m_GyroAmplifyCoeff;
        ScreenGyroPointer.y += auxGyroY * m_GyroAmplifyCoeff;

        m_LastOrientation = m_NewOrientation;


    }

    /// <summary>
    /// Recenters the gyro pointer to the center of the screen
    /// </summary>
    void move_RecenterGyroPointer(object sender, System.EventArgs e)
    {
        ScreenGyroPointer = m_CenterScreen;
    }

    private void OnGUI()
    {
        if (PSMoves.Count > 0)
        {
            string btnDisplay = "";
            string sensorDisplay = "";
            for (int i = 0; i < PSMoves.Count; i++)
            {
                if (PSMoves[i].IsConnected)
                {
                    btnDisplay = string.Format("PS Move {0} - Connected: Tri[{1}] Cir[{2}] X[{3}] Sq[{4}] Sel[{5}] St[{6}] PS[{7}] Mv[{8}] Tg[{9}]\n",
                        PSMoves[i].PSMoveID,
                        PSMoves[i].IsTriangleButtonDown ? 1 : 0,
                        PSMoves[i].IsCircleButtonDown ? 1 : 0,
                        PSMoves[i].IsCrossButtonDown ? 1 : 0,
                        PSMoves[i].IsSquareButtonDown ? 1 : 0,
                        PSMoves[i].IsSelectButtonDown ? 1 : 0,
                        PSMoves[i].IsStartButtonDown ? 1 : 0,
                        PSMoves[i].IsPSButtonDown ? 1 : 0,
                        PSMoves[i].IsMoveButtonDown ? 1 : 0,
                        PSMoves[i].TriggerValue);

                    sensorDisplay = string.Format("Acc[{0:F2}, {1:F2}, {2:F2}] Gyro[{3:F2}, {4:F2}, {5:F2}] Mag[{6:F2}, {7:F2}, {8:F2}]\n",
                        PSMoves[i].Accelerometer.x,
                        PSMoves[i].Accelerometer.y,
                        PSMoves[i].Accelerometer.z,
                        PSMoves[i].Gyroscope.x,
                        PSMoves[i].Gyroscope.y,
                        PSMoves[i].Gyroscope.z,
                        PSMoves[i].Magnetometer.x,
                        PSMoves[i].Magnetometer.y,
                        PSMoves[i].Magnetometer.z);
                }
                else
                {
                    btnDisplay = string.Format("PS Move {0} - NOT CONNECTED", PSMoves[i].PSMoveID);
                }

                GUI.Label(new Rect(10, 10 + PSMoves[i].PSMoveID * 50, 500, 100), btnDisplay);
                GUI.Label(new Rect(10, 35 + PSMoves[i].PSMoveID * 50, 500, 100), sensorDisplay);
            }

        }
    }


}
