﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------

/*! \file Launcher.cs
 *  \brief The launcher that will load the toolbox.
 * 
 *  The idea of this launcher is that it instantiates at runtime the Toolbox, and the Toolbox instantiates as a child the gameManager.
 * */
using UnityEngine;
using System.Collections;

/// <summary>
/// This class will instantiate a Toolbox in runtime
/// </summary>
[AddComponentMenu("CarlosFramework/Launcher")]
public class Launcher : MonoBehaviour
{

    /// <summary>
    /// Definition of the enum of the location of the GameManager
    /// </summary>
    public enum GameManagerLocationEnum
    {
        Prefab,
        Scene
    }
    /// <summary>
    /// (Field) The location of the GameManager
    /// </summary> 
    [SerializeField] 
    private GameManagerLocationEnum m_GameManagerLocation;

    /// <summary>
    /// (Field) The GameManager located in the scene
    /// </summary>
    [SerializeField]
    private GameObject m_SceneGameManager;

    public void Awake()
    {
        // We create a GameManager instance that will follow the Toolbox pattern
        Toolbox toolbox = Toolbox.Instance;
        //Debug.Log("Toolbox instantiated");
        switch (m_GameManagerLocation)
        {
            case GameManagerLocationEnum.Prefab:
                toolbox.GameManagerLocation = Toolbox.GameManagerLocationEnum.Prefab;
                break;
            case GameManagerLocationEnum.Scene:
                toolbox.GameManagerLocation = Toolbox.GameManagerLocationEnum.Scene;
                // We set the referenced GameManagerObject
                toolbox.GameManagerObject = m_SceneGameManager;
                break;
            default:
                break;
        }

    }

    public void Start()
    {
        
    }

}
