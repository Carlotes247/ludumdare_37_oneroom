﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------

/*! This script handles the life of all the entities in the game */

using UnityEngine;
using System.Collections;
using ReusableMethods;
using UnityEngine.UI;
using UnityEngine.Events;

/// <summary>
/// The object life controller. Controls current life, adding and removing
/// </summary>
[AddComponentMenu("CarlosFramework/LifeController")]
public class LifeController : MonoBehaviour
{

    /// <summary>
    /// (Field) The Object Manager so that this script has acces to the rest of components
    /// </summary>
    private ObjectManager m_ObjectManager;
    /// <summary>
    /// (Property) The Object Manager so that this script has acces to the rest of components
    /// </summary>
    public ObjectManager ObjectManager { get { return this.m_ObjectManager; } set { this.m_ObjectManager = value; } }
    

    /// <summary>
    /// (Field) The life of the gameObject
    /// </summary>
    [SerializeField]
    float m_Life;
    /// <summary>
    /// (Property) The life of the gameObject
    /// </summary>
    float Life
    {
        get
        {
            return m_Life;
        }
        set
        {
            m_Life = value;
        }
    }
    /// <summary>
    /// (Field) The original life that the current object was having
    /// </summary>
    float m_OriginalLife;

    /// <summary>
    /// (Property) The normalized life of the current object
    /// </summary>
    public float NormalizedLife { get { return Normalization.Normalize(m_Life, 0f, m_OriginalLife); } }
    /// <summary>
    /// (Field) The UI life to show in-game 
    /// </summary>
    [SerializeField]
    private Image m_UILife;
    /// <summary>
    /// (Field) The valuesto update on the UILife
    /// </summary>
    private Vector3 m_UILifeValues;
    /// <summary>
    /// (Field) The GameObject containing all the parts of the UILife
    /// </summary>
    [SerializeField]
    private GameObject m_UILifeCanvasGameObject;

    /// <summary>
    /// (Field) The score is giving to kill an enemy
    /// </summary>
    [SerializeField]
    float m_ScoreWhenKilled;
    /// <summary>
    /// (Property) The score is giving to kill an enemy
    /// </summary>
    public float ScoreWhenKilled { get { return this.m_ScoreWhenKilled; } set { this.m_ScoreWhenKilled = value; } }

    /// <summary>
    /// (Field) The list of events to fire when the enemy gets killed
    /// </summary>
    [SerializeField, Header ("Death Setup")]
    private UnityEvent m_EventListWhenKilled;

    /// <summary>
    /// (Field) Definition of the enum of DeathAnimTypes
    /// </summary>  
    private enum DeathAnimTypeEnum
    {
        None,
        Animation,
        EnableOther
    }
    /// <summary>
    /// (Field) The type of death active
    /// </summary>
    [SerializeField]
    private DeathAnimTypeEnum m_DeathAnimType;
    /// <summary>
    /// (Field) The current active 3D Model
    /// </summary>
    [SerializeField]
    private GameObject m_Current3DModel;
    /// <summary>
    /// (Field) The object to enable when the current dies
    /// </summary>
    [SerializeField]
    private GameObject m_ObjectToEnableWhenDead;

    /// <summary>
    /// (Field) Controls if the enemy is alive or not
    /// </summary>
    private bool m_CurrentAlive;

    /// Awake is called when the script instance is being loaded
    public void Awake()
    {
      
        // We find the EnemyManager
        ObjectManager = GetComponent<ObjectManager>();

        // We save the life into originalLife. Later, in the next event, we will load it
        m_OriginalLife = m_Life;

        // We search for the uiLifeCanvasGameObject if the object is an enemy
        if (m_ObjectManager.ObjectTransform.CompareTag("Enemy") && m_UILifeCanvasGameObject == null)
        {
            m_UILifeCanvasGameObject = transform.Find("EnemyCanvas").gameObject;
        }
    }

    // This function is called when the object becomes enabled and active
    public void OnEnable()
    {
        // Load the default component values
        Initialize();
    }

    /// Use this for initialization
    void Start()
    {
        
    }

    ///// Update is called once per frame
    //void Update()
    //{
    //    LifeCycle();
    //    //float lerp = Mathf.PingPong(Time.time, duration) / duration;
    //    //objectRenderer.material.color = Color.Lerp(materialColor, Color.red, lerp); 
    //}

    /// <summary>
    /// Initializes the component to the default values
    /// </summary>
    private void Initialize()
    {
        // We set the object alive
        m_CurrentAlive = true;

        // If the UILife is not null...
        if (m_UILifeCanvasGameObject != null)
        {
            // We make sure the the UILifeCanvas is activated
            m_UILifeCanvasGameObject.SetActive(true); 
        }
        // Every time we enable the object, we reset the life to the original one
        Life = m_OriginalLife;
        // We update the UILife, to show the right value
        UpdateUILife();

        // If the object to enable when dead is not null...
        if (m_ObjectToEnableWhenDead != null)
        {
            // We make sure that the other object for the death anim is disabled
            m_ObjectToEnableWhenDead.SetActive(false);
        }

        switch (m_DeathAnimType)
        {
            case DeathAnimTypeEnum.None:
                break;
            case DeathAnimTypeEnum.Animation:
                // We make sure that the death animation is not playing
                PlayDeathAnimation(false);
                break;
            case DeathAnimTypeEnum.EnableOther:
                // We enable the main model
                DeactivateModelEnableOther(false);
                break;
            default:
                break;
        }
    }


    /// <summary>
    /// The method for removing life
    /// </summary>
    /// <param name="amount"></param>
    public void RemoveLife(float amount)
    {
        if (Life > 0)
        {
            Life -= amount;
        }
        // We show the animation of the hit
        VisualHit();
        // We update the UILife
        UpdateUILife();

        // We check if the object should die
        LifeCycle();
    }

    /// <summary>
    /// The function that will take care of showing that the object is being damaged
    /// </summary>
    void VisualHit()
    {
        //this.anim.SetTrigger("Damage");

        //this.anim.ResetTrigger("VisualHit");
        // We show the damage animation
        ObjectManager.AnimController.Damage();
        
    }

    /// <summary>
    /// Updates the UILife
    /// </summary>
    private void UpdateUILife()
    {
        if (m_UILife != null)
        {
            // We make a local copy of the UILife values
            m_UILifeValues = m_UILife.transform.localScale;
            // We change the local copy
            m_UILifeValues.x = NormalizedLife;
            // We update the UILife scaleValues from the local copy
            m_UILife.transform.localScale = m_UILifeValues;
            //Debug.Log(Vector3.right * NormalizedLife);
        }
    }

    /// <summary>
    /// The method for controlling the lifeCycle of the gameObject
    /// </summary>
    void LifeCycle()
    {
        // If the life is inferior or equal to 0, and is Alive, the object dies
        if (Life <= 0 && m_CurrentAlive)
        {
            Kill(this.gameObject);
        }
    }

    /// <summary>
    /// The method for killing the object
    /// </summary>
    /// <param name="target"></param>
    void Kill(GameObject target)
    {
        // We set alive flag to false
        m_CurrentAlive = false;
        // We increment the score
        Toolbox.Instance.GameManager.ScoreController.UpdateScore(ScoreWhenKilled);
        //Destroy(target);
        // We call all the events from the event list
        m_EventListWhenKilled.Invoke();
        switch (m_DeathAnimType)
        {
            case DeathAnimTypeEnum.None:
                // We deactivate the object
                Deactivate(target);
                break;
            case DeathAnimTypeEnum.Animation:
                // We play the death animation, deactivating some components
                PlayDeathAnimation(true);
                break;
            case DeathAnimTypeEnum.EnableOther:                
                // We deactivate the object and enable other
                DeactivateModelEnableOther(true);
                break;
            default:
                break;
        }
        
    }

    /// <summary>
    /// Method for deactivating the gameObject
    /// </summary>
    private void Deactivate (GameObject target)
    {
        target.SetActive(false);
    }

    /// <summary>
    /// Deactivates the current 3DModel and Activates the other for the animation
    /// </summary>
    /// <param name="value">True to deactivate and change to other, False to activate current again</param>
    public void DeactivateModelEnableOther(bool value)
    {
        // We deactivate the AI Controller to stop moving
        m_ObjectManager.AIController.enabled = !value;
        // We completely stop the object
        m_ObjectManager.MovementController.TotalStop();
        // We disable the collider of the objectManager
        m_ObjectManager.GetComponent<Collider>().enabled = !value;
        // We deactivate the UILifeGameObject
        m_UILifeCanvasGameObject.SetActive(!value);

        // We deactivate the current 3Dmodel
        m_Current3DModel.SetActive(!value);

        // We activate other object
        m_ObjectToEnableWhenDead.SetActive(value);
    }


    /// <summary>
    /// Plays the death animation, deactivating all the necessary objects in place
    /// </summary>
    ///<param name="value">True to play and deactivate stuff, False to activate current again and not play</param>
    private void PlayDeathAnimation (bool value)
    {
        // We deactivate the AI Controller to stop moving
        m_ObjectManager.AIController.enabled = !value;
        // We completely stop the object
        m_ObjectManager.MovementController.TotalStop();
        // We disable the collider of the objectManager
        m_ObjectManager.GetComponent<Collider>().enabled = !value;
        // We deactivate the UILifeGameObject
        m_UILifeCanvasGameObject.SetActive(!value);

        // We play the death animation
        m_ObjectManager.AnimController.Die(value);
    }


}
