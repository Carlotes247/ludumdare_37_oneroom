﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Handles the Dash logic
/// </summary>
public class DashCtrl : MonoBehaviour
{

    /// <summary>
    /// The maximum amount of Dash
    /// </summary>
    [SerializeField, Range(0, 20)]
    private float m_DashPointsMax;
    /// <summary>
    /// The current amount of Dash
    /// </summary>
    [SerializeField]
    private float m_DashPointsCurrent;
    /// <summary>
    /// Seconds to reload dash indicator
    /// </summary>
    [SerializeField, Range (0, 3)]
    private float m_SecondsDashReload;
    /// <summary>
    /// The timer for the Dash Reload
    /// </summary>
    private TimerController m_DashReloadTimer;

    /// <summary>
    /// (Field) Instance of the coroutine
    /// </summary>
    private IEnumerator m_ReloadCoroutine;
    /// <summary>
    /// (Field) Flag that controls if the coroutine is running
    /// </summary>
    private bool m_ReloadCoroutineFlag;

    /// <summary>
    /// (Field) The dash bar UI
    /// </summary>
    [SerializeField]
    private GameObject m_UIDashBar;
    /// <summary>
    /// (Field) The instantiated progress of the dash  bar
    /// </summary>
    private Vector3 m_UIDashBarProgress;

    /// <summary>
    /// Awake executes before Start()
    /// </summary>
    public void Awake()
    {
        Initialize();
    }

    /// <summary>
    /// Initialize all our variables
    /// </summary>
    private void Initialize()
    {
        // Initialize reload timer
        m_DashReloadTimer = this.gameObject.AddComponent<TimerController>();
        m_DashReloadTimer.ObjectLabel = "Dash Reload Timer";

        // Fill the dash points bar
        m_DashPointsCurrent = m_DashPointsMax;

    }

    /// <summary>
    /// Dash the object specified from posFromDash to posToDash.
    /// </summary>
    /// <param name="posFromDash">Position origin from dash.</param>
    /// <param name="posToDash">Position to dash.</param>
    public bool Dash(Vector3 posFromDash, Vector3 posToDash, Rigidbody objectRigidbody)
    {
        // If there are still dash points left, we perform the action
        if (m_DashPointsCurrent > 0)
        {
            // We calculate the normalize dir direction
            Vector3 dashDir = ReusableMethods.Vectors.CalculateDirection(posFromDash, posToDash).normalized;

            // Calculate the force
            Vector3 forcetoDash = dashDir * objectRigidbody.mass * 100f;
            // We add a huge Explosive force in that direction
            objectRigidbody.AddForce(forcetoDash);
            
            // We update the amount of points left
            UpdateDashPoints(-1f * Time.deltaTime);

            return true;
        }
        // If there dash is depleted, we start the reload coroutine
        else
        {
            // We start the reload coroutine
            Toolbox.Instance.GameManager.CoroutineController.StartCoroutineFlag(ReloadDash(m_SecondsDashReload), ref m_ReloadCoroutine, ref m_ReloadCoroutineFlag);

            return false;
        }
        

    }

    /// <summary>
    /// Updates the dash points with an amount
    /// </summary>
    /// <param name="amount"> The amount of points to update</param>
    private void UpdateDashPoints(float amount)
    {
        // If the amount of points is between the boundaries...
        if (m_DashPointsCurrent >= 0 && m_DashPointsCurrent <= m_DashPointsMax)
        {
            // We update the amount of Dashpoints
            m_DashPointsCurrent += amount;

            // Update the dash UI
            UpdateUIDashBar(ReusableMethods.Normalization.Normalize(m_DashPointsCurrent, 0f, m_DashPointsMax));

            // If its below 0, we set it to 0
            if (m_DashPointsCurrent < 0)
            {
                m_DashPointsCurrent = 0;
            }
            // If it is above max, we set it to max
            else if (m_DashPointsCurrent > m_DashPointsMax)
            {
                m_DashPointsCurrent = m_DashPointsMax;
            }
        }
    }

    /// <summary>
    /// Reloads the dash bar. Is a coroutine
    /// </summary>
    /// <param name="timeToReload"> seconds to reload</param>
    /// <returns></returns>
    private IEnumerator ReloadDash(float timeToReload)
    {
        // We wait one frame 
        yield return null;

        while (true)
        {
            // We wait for the time to reload
            if (m_DashReloadTimer.GenericCountDown(timeToReload))
            {
                // We replete the dash with the max size of the dash
                m_DashPointsCurrent = m_DashPointsMax;
             
                // We update the reload bar progress in the HUD to 1 because the reload ended and is full
                UpdateUIDashBar(1f);

                // We make sure that the timer stops for the next time to be called without issues
                m_DashReloadTimer.StopTimer();

                // Before exiting the coroutine, we set the flag to false so that it can start again
                m_ReloadCoroutineFlag = false;

                // We exit the coroutine
                yield break;
            }
            // if it is still reloading...
            else
            {
                // We update the reload bar progress in the UI
                UpdateUIDashBar(m_DashReloadTimer.NormalizedTimer);
            }

            // We wait one frame
            yield return null;
        }

    }

    /// <summary>
    /// Public method to update the ammo reload bar
    /// </summary>
    /// <param name="progress"> Progress to set. 0 empty, 1 full.</param>
    public void UpdateUIDashBar(float progress)
    {
        // We update the local copy of the progress
        m_UIDashBarProgress = m_UIDashBar.transform.localScale;
        // We set the x value to the progress passed in
        m_UIDashBarProgress.x = progress;
        // We set it to the ammo reload bar in the HUD
        m_UIDashBar.transform.localScale = m_UIDashBarProgress;
    }

	/// <summary>
	/// Resets the dash to max.
	/// </summary>
	public void ResetDashToMax () {
		// We reset it
		m_DashPointsCurrent = m_DashPointsMax;

		// We update the UI
		UpdateUIDashBar(1);
	}

}
