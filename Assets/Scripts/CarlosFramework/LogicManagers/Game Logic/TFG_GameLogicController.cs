﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;
using ReusableMethods;

[AddComponentMenu("CarlosFramework/TFG_GameLogicController")]
public class TFG_GameLogicController : GameLogicController {

    /// <summary>
    /// (Field) True if the game is lost
    /// </summary>
    private bool m_LooseFlag;
    /// <summary>
    /// (Property) True if the game is lost
    /// </summary>
    public override bool LooseFlag { get { return this.m_LooseFlag; } }

    /// <summary>
    /// (Field) True if the game is won
    /// </summary>
    private bool m_WinFlag;
    /// <summary>
    /// (Property) True if the game is won
    /// </summary>
    public override bool WinFlag { get { return this.m_WinFlag; } }


    /// <summary>
    /// (Field) Array containing all the groups of enemies
    /// </summary>
    [SerializeField]
    private EnemyGroupController[] m_EnemyGroups;
    /// <summary>
    /// (Field) Will contain how many groups of enemies have been defeated already
    /// </summary>
    private bool[] m_EnemyGroupsDefeated;

    // Use this for initialization
    void Start () {
        // We initalize the game
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Initializes the component to the default values
    /// </summary>
    public void Initialize ()
    {
        // We set all the flags to false
        m_LooseFlag = false;
        m_WinFlag = false;

        // We open the mainMenu
        Toolbox.Instance.GameManager.MenuController.OpenMainMenu(true);
    }

    /// <summary>
    /// Loose then game when called
    /// </summary>
    public override void Loose()
    {
        // We set the loose flag to true
        m_LooseFlag = true;
        Debug.Log("Game Lost");
    }

    /// <summary>
    /// Wins then game when called
    /// </summary>
    public override void Win()
    {
        if (!m_WinFlag)
        {
            // If all the levels have been completed...
            if (Array.TrueForAll(Toolbox.Instance.GameManager.GameLevelController.GameLevels, GameLevelLogic.CheckLevelCompleted))
            {
                // We set the win flag to true
                m_WinFlag = true;
                Debug.Log("Game Won");
                Toolbox.Instance.GameManager.HudController.UpdateStoryText("Game Won"); 
            }
            // If there are levels left to complete...
            else
            {
                Debug.Log("There are still levels left to complete!");
                Toolbox.Instance.GameManager.HudController.UpdateStoryText("There are still levels left to complete!");
            }
        }
    }

	/// <summary>
	/// [NOT IMPLEMENTED] Wins then game when called, passing in which player
	/// </summary>
	/// <param name="playerID">Player ID.</param>
	public override void Win (int playerID)
	{
		throw new NotImplementedException ();
	}

    /// <summary>
    /// Starts the game when called
    /// </summary>
    public override void StartGame()
    {
        // We close the Main Menu
        Toolbox.Instance.GameManager.MenuController.OpenMainMenu(false);
        // We load the first level
        Toolbox.Instance.GameManager.GameLevelController.LoadGameLevel(0);
        // We place the player in the initial position [DEPRECATED- Now it is responsibality of the GameLevelLogic]
        //Toolbox.Instance.GameManager.Player.MovementController.Teleport(m_InitialPlayerPosition);
    }

    /// <summary>
    /// Spawns a group of enemies specified by an index
    /// </summary>
    /// <param name="index"> The index of the group to spawn</param>
    public void SpawnEnemyGroup(int index)
    {
        // We activate the selected group 
        m_EnemyGroups[index].SpawnGroup();
    }    

    /// <summary>
    /// Sets the player in the initial position
    /// </summary>
    /// <param name="pos"> The position to set the player</param>
    public override void SetPlayerAtInitialPosition(Vector3 pos)
    {
        // We teleport the player to the position passed in
        Toolbox.Instance.GameManager.Player.MovementController.Teleport(pos);

        // We make sure that no residual rigidbody speed is in the player
        Toolbox.Instance.GameManager.Player.ObjectRigidbody.velocity = Vector3.zero;
        Toolbox.Instance.GameManager.Player.ObjectRigidbody.angularVelocity = Vector3.zero;
        // We reset the rotation of the player
        Toolbox.Instance.GameManager.Player.RotationController.ResetRotation();
        
    }

	/// <summary>
	/// Sets the players at initial position.
	/// </summary>
	/// <param name="posArray">Position array.</param>
	public override void SetPlayersAtInitialPosition(Vector3[] posArray) {
		for (int i = 0; i < posArray.Length; i++) {
			// We teleport the player to the position passed in
			Toolbox.Instance.GameManager.Players[i].MovementController.Teleport(posArray[i]);

			// We make sure that no residual rigidbody speed is in the player
			Toolbox.Instance.GameManager.Players[i].ObjectRigidbody.velocity = Vector3.zero;
			Toolbox.Instance.GameManager.Players[i].ObjectRigidbody.angularVelocity = Vector3.zero;
			// We reset the rotation of the player
			if (Toolbox.Instance.GameManager.Players[i].RotationController != null) {
				Toolbox.Instance.GameManager.Players[i].RotationController.ResetRotation();
			}
		}
	}
}
