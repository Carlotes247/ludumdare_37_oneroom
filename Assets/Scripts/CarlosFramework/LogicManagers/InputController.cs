﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

/// <summary>
/// The Input Controller, controls the input mapping and input logic of the game
/// </summary>
[AddComponentMenu("CarlosFramework/InputController")]
public class InputController : MonoBehaviour
{

    /// <summary>
    /// Flag to control if the debug is allowed in this class
    /// </summary>
    public bool AllowDebug;

    /// <summary>
    /// (Field) The pointer position in screen coordinates
    /// </summary>
    [SerializeField]
    private Vector3 m_ScreenPointerPos;
    /// <summary>
    /// (Property) The pointer position in screen coordinates
    /// </summary>
    public Vector3 ScreenPointerPos { get { return this.m_ScreenPointerPos; } set { this.m_ScreenPointerPos = value; } }

    /// <summary>
    /// (Field) Flag to know if the pointer is on the screen or not
    /// </summary>
    [SerializeField]
    private bool m_PointerOnScreen;
    /// <summary>
    /// (Property) Flag to know if the pointer is on the screen or not
    /// </summary>
    public bool PointerOnScreen { get { return this.m_PointerOnScreen; } }
    /// <summary>
    /// (Field) The rect of the screen, to know if the pointer is out or not
    /// </summary>
    private Rect m_ScreenRect;

    /// <summary>
    /// (Field) The axis that the actual controller is returning
    /// </summary>
    [SerializeField]
    private Vector2 m_ControllerAxis;
    /// <summary>
    /// (Property) The axis that the actual controller is returning
    /// </summary>
    public Vector2 ControllerAxis { get { return this.m_ControllerAxis; } }

    /// The Wiimote Input received by the computer
    // Field
    [SerializeField]
    private WiiMoteInput wiimoteInput;
    // Property
    public WiiMoteInput WiimoteInput { get { return this.wiimoteInput; } set { this.wiimoteInput = value; } }

    /// <summary>
    /// (Field) The Arduino Input Controller
    /// </summary>
    [SerializeField]
    private ArduinoInputController m_ArduinoInput;
    /// <summary>
    /// (Property) The Arduino Input Controller
    /// </summary>
    public ArduinoInputController ArduinoInput { get { return this.m_ArduinoInput; } }

    /// <summary>
    /// The array of PSmoves available
    /// </summary>
    [SerializeField, Header("PSMove Setup")]
    private PSMoveController[] m_Moves;
    public PSMoveController[] Moves { get { return m_Moves; } }

    /// <summary>
    /// The input controller for the psmoves
    /// </summary>
    [SerializeField]
    private PSMoveInputCtrl PSMoveInput;

    /// <summary>
    /// Filtered position for each PSMove
    /// </summary>
    [SerializeField]
    private Vector3[] MovesFiltered;
    private Vector3[] m_AuxFilteredPositions;
    /// <summary>
    /// The filtered and lerped positions of the PSMoves
    /// </summary>
    [SerializeField]
    public Vector3[] MovesLerpedFilteredPos;
    /// <summary>
    /// The timer for the lerping
    /// </summary>
    private TimerController m_FilterLerpingTimer;
    /// <summary>
    /// The amount of seconds the lerp will take
    /// </summary>
    [Range(0f, 1f)]
    public float m_SecondsToMakeLerp;
    /// <summary>
    /// Flag that will control when the lerp was completed
    /// </summary>
    [SerializeField]
    private bool m_LerpCompleted;
    /// <summary>
    /// The original position of the unfiltered tracker
    /// </summary>
    [SerializeField]
    private Vector3[] m_OriginalTrackerPos;


    /// <summary>
    /// Number of frames to filter PSMove Position (we will calculate the average of the pos)
    /// </summary>
    public int NumFramesToFilter;
    /// <summary>
    /// The current frame in the filtering (the max will be numFramesTofilter)
    /// </summary>
    private int m_CurrentFrameInFilter;
    [SerializeField, Range(0f, 1f)]
    private float m_FilterLerpFactor;


    /// The input mode that is currently active
    // The enum to decide which control do we have
    public enum TypeOfInput
    {
        Mouse,
        WiiMote,
        PSMove
    }
    // Field
    [SerializeField, Header ("Input Type")]
    private TypeOfInput inputType;
    // Property
    public TypeOfInput InputType
    {
        get { return this.inputType; }
        set { this.inputType = value; }
    }
    
    /// <summary>
    /// (Field) The Axis of the camera scalated to (-1, 1)
    /// </summary>
    private Vector2 m_PointerAxis;
    // [DEPRECATED] Now done in Rotation Controller of the GameManager.MainCamera
    ///// <summary>
    ///// (Property) The Axis of the camera scalated to (-1, 1)
    ///// </summary>
    //private Vector2 CameraAxis { get { return m_PointerAxis; } set { this.m_PointerAxis = value; } }

    ///// <summary>
    ///// (Field) The speed of the rotation of the camera
    ///// </summary>
    //[SerializeField, Range(0f, 1f)]
    //private float m_CameraRotationSpeed;

    /// <summary>
    /// (Field) The rotation of the camera in eulerAngles
    /// </summary>
    [SerializeField]
    private Vector2 m_RotationCamera;
    /// <summary>
    /// (Property) The rotation of the camera in eulerAngles
    /// </summary>
    public Vector2 RotationCamera { get { return this.m_RotationCamera; } set { this.m_RotationCamera = value; } }

    /// <summary>
    /// (Field) The timer of the pointer out of screen, to control when the pointer is centered on screen
    /// </summary>
    private TimerController m_PointerOutScreenTimer;
    /// <summary>
    /// (Field) Seconds needed to position the pointer in the center after the pointer is out of screen
    /// </summary>
    [SerializeField, Range(0f, 5f), Header("Screen Pointer Setup")]
    private float m_SecondsToPointerOutScreen;
    /// <summary>
    /// (Field) Flag to know if we need to put the pointer to the center of the screen
    /// </summary>
    private bool m_PointerToCenter;

    /// <summary>
    /// Definition of the delegate of the wiimote functions
    /// </summary>
    delegate void ControllerDelegate();
    /// <summary>
    /// The delegate of the mouse functions
    /// </summary>
    ControllerDelegate m_MouseDelegate;
    /// <summary>
    /// The delegate of the wiimote functions
    /// </summary>
    ControllerDelegate m_WiiMoteDelegate;
    /// <summary>
    /// The delegate of the PSMove functions
    /// </summary>
    ControllerDelegate m_PSMoveDelegate;
    /// <summary>
    /// The delegate containing all the functions to make work the inputControllerDelegate
    /// </summary>
    ControllerDelegate m_InputControllerDelegate;

    /// <summary>
    /// (Field) The flag that lets the player shoot
    /// </summary>
	private InputCommand m_LeftClickInputCommand;
    /// <summary>
    /// (Property) The flag that lets the player shoot
    /// </summary>
    public InputCommand LeftClickInputCommand { get { return this.m_LeftClickInputCommand; } }    

	/// <summary>
	/// Input Commands enumeration
	/// </summary>
	public enum InputCommandsEnum
	{
        Nothing,
        Shoot,
		Dash
	}
	/// <summary>
	/// The mouse left click command enum.
	/// </summary>
	[SerializeField, Tooltip("The mouse left click command")]
	private InputCommandsEnum m_LeftClickCommand;

    /// <summary>
    /// The offset the controller will have from the camera (if it is in 3d)
    /// </summary>
    [SerializeField, Range(0f, 5f), Tooltip("The offset the controller will have from the camera (if it is in 3d)")]
    private float m_CameraOffset;
    public float CameraOffset { get { return m_CameraOffset; } }

    /// <summary>
    /// Flag for the generic trigger down (similar to mouse down)
    /// </summary>
    [SerializeField]
    private bool m_TriggerPressedDown;
    /// <summary>
    /// Flag for the generic trigger down (similar to mouse down)
    /// </summary>
    public bool TriggerPressedDown { get { return m_TriggerPressedDown; } }

    /// <summary>
    /// Flag for trigger pressed (hold)
    /// </summary>
    [SerializeField]
    private bool m_TriggerPressed;
    /// <summary>
    /// Flag for trigger pressed (hold)
    /// </summary>
    public bool TriggerPressed { get { return m_TriggerPressed; } }

    /// <summary>
    /// The timer for the trigger pressed down
    /// </summary>
    private TimerController m_TimerTriggerDown;
    /// <summary>
    /// The amount of time the trigger is available as down 
    /// </summary>
    [SerializeField, Range(0f, 0.02f)]
    float m_SecsTriggerPressedDown = 0.3f;


    // Awake is called when the script instance is being loaded
    public void Awake()
    {        
        // We initialize the timer component 
        m_PointerOutScreenTimer = this.gameObject.AddComponent<TimerController>();
        // We label it to identify it in the inspector
        m_PointerOutScreenTimer.ObjectLabel = "InputControllerPointerOutScreenTimer";
    }

    // Use this for initialization
    void Start()
    {
        //WiimoteInput.WiimoteInputLogic();

        // We set always mouse on android to avoid invoking the UniWii dll
#if UNITY_ANDROID
        this.InputType = TypeOfInput.Mouse;
#endif
        // We initialize the LeftInputCommand if it is null
        if (m_LeftClickInputCommand == null)
        {
			switch (m_LeftClickCommand) {
                case InputCommandsEnum.Nothing:
                    m_LeftClickInputCommand = new EmptyCommand();
                    break;
                case InputCommandsEnum.Shoot:
				    m_LeftClickInputCommand = new ShootCommand();
				    break;
			    case InputCommandsEnum.Dash:
				    m_LeftClickInputCommand = new DashCommand ();
				    break;
			default:
				break;
			}

        }

        // We assign the functions to the delegates        
        PopulateMouseDelegate();
        PopulateWiimoteDelegate();
        PopulatePSmoveDelegate();

        // We update the InputController delegate depending on the inputType
        switch (InputType)
        {
            case TypeOfInput.Mouse:
                SetMouseControl();
                break;
            case TypeOfInput.WiiMote:
                SetWiimoteControl();
                break;
            case TypeOfInput.PSMove:
                SetPSMoveControl();
                break;
            default:
                break;
        }

        // Add the timer controller for the trigger down
        m_TimerTriggerDown = this.gameObject.AddComponent<TimerController>();
        m_TimerTriggerDown.ObjectLabel = "Timer Trigger Down Input Ctrl";
        // Add timer controller for filtering psmoves
        m_FilterLerpingTimer = this.gameObject.AddComponent<TimerController>();
        m_FilterLerpingTimer.ObjectLabel = "Timer for PSMove filtering";

        // Make sure that the array of filtered moves is the same size as the array of moves
        if (MovesFiltered.Length != m_Moves.Length)
        {
            MovesFiltered = new Vector3[m_Moves.Length];
            m_AuxFilteredPositions = new Vector3[m_Moves.Length];
        }
        if (MovesLerpedFilteredPos.Length != m_Moves.Length)
        {
            MovesLerpedFilteredPos = new Vector3[m_Moves.Length];
        }
        if (m_OriginalTrackerPos.Length != m_Moves.Length)
        {
            m_OriginalTrackerPos = new Vector3[m_Moves.Length];
        }

        for (int i = 0; i < m_Moves.Length; i++)
        {
            // We set a new original position
            m_OriginalTrackerPos[i] = m_Moves[i].transform.position;
        }

    }


    // Update is called once per frame
    void Update()
    {
        // The InputControllerDelegate runs the logic, depending on the inputType
        m_InputControllerDelegate();
        //// We check the type of input for any changes
        ////CheckInputType();
        //// We update the values of the screenPointerPos
        //UpdateScreenPointerPos();
        //// We update the values of the controllerAxis
        //UpdateControllerAxis();
        // We check the PointerOnScreen flag
        CheckPointerOnScreen();
        // We update the values of the camera Axis
        UpdatePointerAxis();
        // We draw the pointer in the specified pos
        DrawScreenPointer(ScreenPointerPos);
        // We check if the user press any buttons
        CheckUserInput();
    }

    /// <summary>
    /// Checks the type of input and assign a delegate for the inputController logic
    /// </summary>
    void CheckInputType()
    {        
        // Depending on the inputType, we assign to the InputControllerDelegate a ControllerDelegate
        switch (InputType)
        {
            case TypeOfInput.Mouse:
                m_InputControllerDelegate = m_MouseDelegate;                
                break;
            case TypeOfInput.WiiMote:
                m_InputControllerDelegate = m_WiiMoteDelegate;
                break;
            case TypeOfInput.PSMove:
                // TODO set psmove delegate
                // we set the mouse delegate for the moment
                m_InputControllerDelegate = m_PSMoveDelegate;
                break;
            default:
                break;
        }

    }

    /// Update ScreenPointer position
    void UpdateScreenPointerPos()
    {
        switch (InputType)
        {
            case TypeOfInput.Mouse:
                UpdateScreenPointerMouse();
                break;
            case TypeOfInput.WiiMote:
                // We run the logic of the wiimote, to populate all the variables
                //WiimoteInput.WiimoteInputLogic();
                // We get the position of the wiimote
                UpdateScreenPointerWiimote();
                break;
            case TypeOfInput.PSMove:
                // We get the position of the psmove
                UpdateScreenPointerPSMove();
                break;
            default:
                Debug.LogError("Type of Input not recognized!");
                break;
        }
    }

    /// <summary>
    /// Updates the screenPointer from the mouseCursor
    /// </summary>
    private void UpdateScreenPointerMouse()
    {
        ScreenPointerPos = Input.mousePosition;
    }

    /// <summary>
    /// Updates the screenPointer from the wiimotePointer
    /// </summary>
    private void UpdateScreenPointerWiimote()
    {
        // We get the position of the wiimote
        ScreenPointerPos = WiimoteInput.CursorPosition;
    }

    /// <summary>
    /// Updates the screenPointer from the 1st PSMove
    /// </summary>
    private void UpdateScreenPointerPSMove()
    {
        if (PSMoveManager.GetManagerInstance().TrackerEnabled)
        {
            ScreenPointerPos = Toolbox.Instance.GameManager.MainCamera.CameraComponent.WorldToScreenPoint(m_Moves[0].transform.position);
        }
        else
        {
            ScreenPointerPos = PSMoveInput.ScreenGyroPointer;
            Debug.Log("PSMove gyro used for pointer!");
        }

    } 

    /// <summary>
    /// Checks if the mousePointer is on screen, and updates the pointerOnScreen flag
    /// </summary>
    private void CheckMousePointerOnScreen()
    {
        // We update the width and height of the screen        
        m_ScreenRect.width = Screen.width;
        m_ScreenRect.height = Screen.height;
        //m_ScreenRect = new Rect(0, 0, Screen.width, Screen.height);

        // We update the pointerOnScreen flag depending if the pointer is on screen or not        
        m_PointerOnScreen = m_ScreenRect.Contains(m_ScreenPointerPos);
        // If the pointer is below a big approximation to 0
        if (m_ScreenPointerPos.x < 0.00001f || m_ScreenPointerPos.y < 0.00001f)
        {
            m_PointerOnScreen = false;
        }

    }

    /// <summary>
    /// Checks if the WiimotePointer is on screen, and updates the pointerOnScreen flag
    /// </summary>
    private void CheckWiimotePointerOnScreen()
    {
        // We update the pointerOnScreen flag asi if it was the mouse one
        CheckMousePointerOnScreen();
    }

    private void CheckPSMovePointerOnScreen()
    {
        // If the tracker is enabled...
        if (PSMoveManager.GetManagerInstance().TrackerEnabled)
        {
            // We update the pointerOnScreen flag depending if the PSMove is seen by tracker or not
            m_PointerOnScreen = m_Moves[0].IsTracking;
        }
        // If not, we use the same logic as the mouse
        else
        {
            CheckMousePointerOnScreen();
        }
    }

    /// <summary>
    /// Checks if the pointer is on Screen. If not, sets the Pointer values to center of screen
    /// </summary>
    private void CheckPointerOnScreen()
    {
        //Debug.Log("The pointer is OnScreen? " + m_PointerOnScreen);
        // If the pointer is not on screen...
        if (!m_PointerOnScreen)
        {
            if (m_PointerToCenter)
            {
                // We set the screenPointerPos to center of screen
                m_ScreenPointerPos.x = m_ScreenRect.width * 0.5f;
                m_ScreenPointerPos.y = m_ScreenRect.height * 0.5f;

                // We break the if
                return;
            }
            // After X seconds...
            if (m_PointerOutScreenTimer.GenericCountDown(m_SecondsToPointerOutScreen))
            {
                // We set the flag pointerToCenter to true
                m_PointerToCenter = true;                
            }
        }
        // If the pointer is on screen...
        else
        {
            // We set the screenPointerPos to center of screen
            m_PointerToCenter = false;
            // We make sure that the timer is stopped, so next time is counting the right amount of seconds, and not less
            m_PointerOutScreenTimer.StopTimer();
        }
    }

    /// <summary>
    /// Updates the values of the controller Axis
    /// </summary>
    private void UpdateControllerAxis()
    {
        switch (InputType)
        {
            case TypeOfInput.Mouse:
                // We get the input from the keyboard
                UpdateAxisWASD();
                break;
            case TypeOfInput.WiiMote:
                // We get the input from the WiimoteNunchuck
                UpdateAxisNunchuck();
                break;
            case TypeOfInput.PSMove:
                // TODO updateAxisPSMove
                // do the mouse for the moment
                UpdateAxisWASD();
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// Updates the ControllerAxis from the WASD keys in the keyboard
    /// </summary>
    private void UpdateAxisWASD()
    {
        // We get the input from the keyboard
        // X axis
        if (Input.GetKey(KeyCode.D))
        {
            m_ControllerAxis.x = 1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            m_ControllerAxis.x = -1;
        }
        else
        {
            m_ControllerAxis.x = 0;
        }
        // Y axis
        if (Input.GetKey(KeyCode.W))
        {
            m_ControllerAxis.y = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            m_ControllerAxis.y = -1;
        }
        else
        {
            m_ControllerAxis.y = 0;
        }
    }

    /// <summary>
    /// Updates the axis from the nunchuck joystick
    /// </summary>
    private void UpdateAxisNunchuck()
    {
        // We get the input from the WiimoteNunchuck
        m_ControllerAxis = WiimoteInput.NunchuckJoystickValues;
    }

    /// <summary>
    /// Updates the Camera Axis to a value between (-1, 1)
    /// </summary>
    private void UpdatePointerAxis()
    {
        // X is the Scalated from the normalized screenPointerPos and the screen width
        m_PointerAxis.x = ReusableMethods.Normalization.ScaleNormalize((ReusableMethods.Normalization.Normalize(ScreenPointerPos.x, 0, Screen.width)), -1, 1);
        // Y is the Scalated from the normalized screenPointerPos and the screen height
        m_PointerAxis.y = ReusableMethods.Normalization.ScaleNormalize((ReusableMethods.Normalization.Normalize(ScreenPointerPos.y, 0, Screen.height)), -1, 1);
        //Debug.Log("Pointer axis are: " + m_PointerAxis.ToString());

    }

    /// <summary>
    /// The function in charge of Drawing on Screen a pointer (can be used by the any source of input, including mouse, Wiimote, PSMove, etc)
    /// </summary>
    /// <param name="values"> Values of the pointer in screen coordinates</param>
    void DrawScreenPointer(Vector3 values)
    {
        Toolbox.Instance.GameManager.HudController.InGameCursor.transform.position = new Vector3(values.x, values.y, 0);
    }

    void DrawScreenPointer(int value_x, int value_y)
    {
        Toolbox.Instance.GameManager.HudController.InGameCursor.transform.position = new Vector3(value_x, value_y, 0);
    }

    /// <summary>
    /// The function to check the user Input
    /// </summary>
    private void CheckUserInput()
    {
        // If the Menu Is not Open...
        if (!Toolbox.Instance.GameManager.MenuController.MenuOpen)
        {
            // We checl for ESC input to open the menu if the game is not won
			if (Input.GetKeyDown(KeyCode.Escape) && !Toolbox.Instance.GameManager.GameLogicController.WinFlag)
            {
                // We open the Menu and pause the game
                Toolbox.Instance.GameManager.MenuController.PauseGame(true);
            }
            
            // We check for main click input
            if ((Input.GetMouseButton(0) || WiimoteInput.ButtonB || ArduinoInput.ButtonArduino ))
            {
                // We call the left Click command and execute it                              
                m_LeftClickInputCommand.Execute(Toolbox.Instance.GameManager.Player);
            }
            else
            {
                // We make sure that shoot command was not called
                m_LeftClickInputCommand.Called = false;
            }

            // If the ShotCommand was succesful in the execution...   
            if (m_LeftClickInputCommand.Success)
            {
                // ... And the wiimote is selected as the input...
                if (InputType == TypeOfInput.WiiMote)
                {
                    // We rumble the wiimote of player 1
                    WiimoteInput.SetWiimoteRumble(0, WiimoteInput.TimeToRumble);
                }

                //// We add the shot to the list in the repeatibility experiment
                //Toolbox.Instance.GameManager.RepeatibilityCtrl.AddShotToList(m_ScreenPointerPos);

                // We set the Success flag to false (we only shoot a bullet a frame)
                m_LeftClickInputCommand.Success = false;
            }

            // Player Movement Input
            // If the player can move from the inputController...
			if (Toolbox.Instance.GameManager.Player.MovementController.TypeOfControl == MovementController.TypeOfControlEnum.InputController)
            {

                if (InputType == TypeOfInput.WiiMote)
                {
                    // We update the player axis depending on motion controlled stuff
                    MotionMovementPlayer();  
                }               
					
				// We switch the type of input movement and update acordingly
				switch (Toolbox.Instance.GameManager.Player.MovementController.TypeOfInputControl) {
					case MovementController.TypeOfInputControlEnum.WASD:
						// ... We move the player according to the axis
						Toolbox.Instance.GameManager.Player.MovementController.MoveInputController(m_ControllerAxis,
							Toolbox.Instance.GameManager.Player.MovementController.MaxVelocity); 
						break;
					case MovementController.TypeOfInputControlEnum.MouseFollow:
					// ... We move the player according to the mouseInput (we can pass screen coordinates, it calculates world coordinates inside the function)
					Toolbox.Instance.GameManager.Player.MovementController.MoveInputController(m_ScreenPointerPos); 
						break;			
					default:
						break;
				}

                // We rotate the player on the Y axis only (horizontal rotation)
                Toolbox.Instance.GameManager.Player.RotationController.Rotate(m_PointerAxis);
                // We rotate the camera on the X axis only (vertical rotation)
                Toolbox.Instance.GameManager.MainCamera.RotationController.Rotate(m_PointerAxis);

                
            }
        }
        // If the menu is open...
        else
        {
            // We checl for ESC input to close the menu
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // We close the Menu and unpause the game
                Toolbox.Instance.GameManager.MenuController.PauseGame(false);
            }
        }
    }

    /// <summary>
    /// Moves the player with motion sensing controls (pitch and accelerometer)
    /// </summary>
    private void MotionMovementPlayer()
    {
        // If Wiimote.Pitch is down
        if (wiimoteInput.PitchDown)
        {
            //Debug.Log("<b> PITCH DOWN!!");
            // If wiimote is shaked...
            if (wiimoteInput.Shake)
            {                
                //... We uptade the axis to move forward at a 100% of the max speed
                m_ControllerAxis = Vector2.up;
                // We end the method here
                return;
            }
            //... We uptade the axis to move forward at a 30% of the max speed
            m_ControllerAxis = Vector2.up * 0.3f;
        }
        // If the pitch is not down...
        else
        {
            // If wiimote is rolled to the left...
            if (wiimoteInput.RollLeft)
            {
                //... We uptade the axis to move left at a 30% of the max speed
                m_ControllerAxis.x = -0.3f;
                // We end the method here
                return;
            }
            // If wiimote is rolled to the right...
            if (wiimoteInput.RollRight)
            {
                //... We uptade the axis to move right at a 30% of the max speed
                m_ControllerAxis.x = 0.3f;
                // We end the method here
                return;
            }
            // If we reach here, there was no roll at all
            // There is no movement left or right
            m_ControllerAxis.x = 0;
        }
    }

    /// <summary>
    /// Resets the rotation of the player and the camera to zero
    /// </summary>
    public void RestartRotationCameraPlayer()
    {
        // We set the rotation to zero
        m_RotationCamera = Vector2.zero;

        // We rotate the player on the X axis only
        Toolbox.Instance.GameManager.Player.ObjectTransform.eulerAngles = new Vector3(
            Toolbox.Instance.GameManager.Player.ObjectTransform.eulerAngles.x,
            m_RotationCamera.y,
            Toolbox.Instance.GameManager.Player.ObjectTransform.eulerAngles.z);
        // We then rotate the camera on the Y axis
        Camera.main.transform.eulerAngles = new Vector3(
            m_RotationCamera.x,
            Camera.main.transform.eulerAngles.y,
            Camera.main.transform.eulerAngles.z);
    }

    /// <summary>
    /// Checks if the generic trigger has been pressed down (only checks once)
    /// </summary>
    /// <param name="triggerValue"> the value of the trigger to check</param>
    /// <param name="threshold"> the limit to check</param>
    /// <returns></returns>
    public bool GetTriggerPressedDown (float triggerValue, float threshold)
    {
        // If the trigger is above the threshold...
        if (triggerValue > threshold)
        {
            // ...and the trigger was not down before....
            if (!TriggerPressed)
            {
                // we set the trigger down
                m_TriggerPressedDown = true;
                // We set the trigger pressed 
                m_TriggerPressed = true;
                // We return the trigger pressed down once
                return true;
            }
            // ...and the trigger was pressed before
            else
            {
                // We only show the trigger pressed down once (similar to mouse down)               
                if (m_TimerTriggerDown.GenericCountDown(m_SecsTriggerPressedDown))
                {
                    m_TriggerPressedDown = false;
                }
                return m_TriggerPressedDown;
            }
            
        }
        // If below threshold...
        else
        {
            // Trigger not down any more
            m_TriggerPressedDown = false;
            // Trigger not pressed any more
            m_TriggerPressed = false;

            // Stop the trigger down timer
            m_TimerTriggerDown.StopTimer();

            return false;
        }

        
    }

    private void FilterAveragePSMovePos ()
    {
        // If there are still frames to average...
        if (m_CurrentFrameInFilter < NumFramesToFilter)
        {
            // We go through all the moves...
            for (int i = 0; i < m_Moves.Length; i++)
            {
                // We a new position to the average
                m_AuxFilteredPositions[i] += m_Moves[i].transform.position;
            }

            m_CurrentFrameInFilter++;
        }
        // If we reached the max of frames to average...
        else
        {
            // We go through all the moves...
            for (int i = 0; i < m_Moves.Length; i++)
            {
                // We divide the result by the total num of frames
                m_AuxFilteredPositions[i] /= NumFramesToFilter;
                // We set the filtered pos
                MovesFiltered[i] = m_AuxFilteredPositions[i];
                // we free the temporalPosition for next average calculation
                m_AuxFilteredPositions[i] = Vector3.zero;
            }

            // reset current frame 
            m_CurrentFrameInFilter = 0;
        }

        // We let the countdown happen for the lerp
        m_FilterLerpingTimer.GenericCountDown(m_SecondsToMakeLerp);

        // We store in which moment of the lerp we are
        m_FilterLerpFactor = m_FilterLerpingTimer.NormalizedTimer;

        // If the lerp has been completed...
        if (m_LerpCompleted)
        {
            for (int i = 0; i < m_Moves.Length; i++)
            {
                // We set a new original position
                m_OriginalTrackerPos[i] = m_Moves[i].transform.position;
            }

            // We set the flag to false
            m_LerpCompleted = false;
        }
        // If the lerp is still ongoing...
        else
        {
            // We check if it is done
            if (m_FilterLerpFactor > 1)
            {
                // We set the flag to true
                m_LerpCompleted = true;
            } 
        }

        // We calculate the lerp
        for (int i = 0; i < m_Moves.Length; i++)
        {
            MovesLerpedFilteredPos[i] = Vector3.Lerp(m_OriginalTrackerPos[i], MovesFiltered[i], m_FilterLerpFactor);
        }
    }

    #region DelegateMethods

    /// <summary>
    /// Sets the control to the Wiimote
    /// </summary>
    public void SetWiimoteControl()
    {
        InputType = TypeOfInput.WiiMote;

        // We start the WiimoteInput
        WiimoteInput.StartWiimoteInput();

        // We update the InputController delegate
        CheckInputType();
    }

    /// <summary>
    /// Sets the control to the mouse
    /// </summary>
    public void SetMouseControl()
    {
        InputType = TypeOfInput.Mouse;
        // We update the InputController delegate
        CheckInputType();
    }

    /// <summary>
    /// Sets the control to the PSMove
    /// </summary>
    public void SetPSMoveControl()
    {
        InputType = TypeOfInput.PSMove;

        // We start the PSMove Service
        PSMoveManager.GetManagerInstance().StartPSMoveService();

        // We update the InputController delegate
        CheckInputType();
    }

    /// <summary>
    /// Test call for mouse
    /// </summary>
    void TestMouse()
    {
        Debug.Log("MouseDelegate called");
        // If this was called, is because was a subscriber from the delegate. We unsubscribe
        m_InputControllerDelegate -= TestMouse;
    }

    /// <summary>
    /// Test call for wiimote
    /// </summary>
    void TestWii()
    {
        Debug.Log("WiiDelegate called");
        // If this was called, is because was a subscriber from the delegate. We unsubscribe
        m_InputControllerDelegate -= TestWii;

    }

    /// <summary>
    /// Populates the mouseDelegate with all the necessary functions
    /// </summary>
    void PopulateMouseDelegate()
    {
        // The debug info on the console
        m_MouseDelegate += TestMouse;
        // We update the screenPointer of the mouse
        m_MouseDelegate += UpdateScreenPointerMouse;
        // We update the flag PointerOnScreen based on mouse input
        m_MouseDelegate += CheckMousePointerOnScreen;
        // We get the input from the WASD keys
        m_MouseDelegate += UpdateAxisWASD;
    }

    /// <summary>
    /// Populates the wiimoteDelegate with all the necessary functions
    /// </summary>
    void PopulateWiimoteDelegate()
    {
        // The debug info on the console
        m_WiiMoteDelegate += TestWii;
        // We update the screenPointer of the wiimote
        m_WiiMoteDelegate += UpdateScreenPointerWiimote;
        // We update the flag PointerOnScreen based on wiimote input
        m_WiiMoteDelegate += CheckWiimotePointerOnScreen;
        // We get the input from the WiimoteNunchuck
        m_WiiMoteDelegate += UpdateAxisNunchuck;
    }

    /// <summary>
    /// Populates the PSMoveDelegate with all the necessary functions
    /// </summary>
    void PopulatePSmoveDelegate()
    {
        // We calculate a filtered pos
        m_PSMoveDelegate += FilterAveragePSMovePos;
        // We update the screenPointer of the wiimote
        m_PSMoveDelegate += UpdateScreenPointerPSMove;
        // We update the flag PointerOnScreen based on wiimote input
        m_PSMoveDelegate += CheckPSMovePointerOnScreen;
        // We get the input from the WASD keys
        m_MouseDelegate += UpdateAxisWASD;
    }

    #endregion


    public void OnDrawGizmos()
    {
        if (AllowDebug)
        {
            // Draw The filtered pos of the PSMoves
            for (int i = 0; i < m_Moves.Length; i++)
            {
                //Gizmos.DrawSphere(MovesLerpedFilteredPos[i], 0.1f);
            } 
        }
    }
}
