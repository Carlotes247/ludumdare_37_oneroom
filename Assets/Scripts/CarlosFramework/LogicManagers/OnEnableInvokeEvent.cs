﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;


/// <summary>
/// Invokes an event when a the GameObject gets enabled
/// </summary>
[AddComponentMenu("CarlosFramework/OnEnableInvokeEvent")]
public class OnEnableInvokeEvent : MonoBehaviour {

    /// <summary>
    /// (Field) Flag to control the call of the trigger once
    /// </summary>
    [SerializeField, Tooltip("If active, will only call the trigger once, even if we disable and enable")]
    private bool m_InvokeOnce;
    /// <summary>
    /// (Field) Was the trigger already called
    /// </summary>
    private bool m_AlreadyInvoked;

    /// <summary>
    /// The event list to invoke 
    /// </summary>
    [SerializeField]
    private UnityEvent m_EventToInvoke;

    // This function is called when the object becomes enabled and active
    public void OnEnable()
    {
        if (!m_AlreadyInvoked)
        {
            m_EventToInvoke.Invoke();

            if (m_InvokeOnce)
            {
                m_AlreadyInvoked = true;
            }
        }
    }

    /// <summary>
    /// Intializes the component's members to default values
    /// </summary>
    private void Inititalize()
    {
        // The trigger was not invoked yer
        m_AlreadyInvoked = false;
    }

}
