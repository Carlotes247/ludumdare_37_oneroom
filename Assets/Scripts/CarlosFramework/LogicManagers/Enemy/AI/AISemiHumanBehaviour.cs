﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;
using ReusableMethods;


/// <summary>
/// Semi Human AI, it will react to the input of the player
/// </summary>
[AddComponentMenu("CarlosFramework/AISemiHumanBehaviour")]
public class AISemiHumanBehaviour : AIBehaviour
{
    #region Fields&Properties
    [SerializeField]
    private ObjectManager objectManager;
    /// <summary>
    /// (Property) The object manager of the current GameObject. Contains all the controllers
    /// </summary>
    public ObjectManager ObjectManager { get { return this.objectManager; } }

    /// <summary>
    /// The definition of the enumeration of personalities of the human
    /// </summary>
    public enum PersonalityEnum
    {
        Coward,
        Prudent,
        Aggresive,
    }
    /// <summary>
    /// (Field) The actual personality of the human
    /// </summary>
    [SerializeField]
    private PersonalityEnum m_Personality;
    /// <summary>
    /// (Property) The actual personality of the human
    /// </summary>s
    public PersonalityEnum Personality { get { return this.m_Personality; } set { this.m_Personality = value; } }

    /// <summary>
    /// (Field) Flag that indicates if we want to use fuzzy logic
    /// </summary>
    [SerializeField, Tooltip("Flag to activate the Fuzzy Logic")]
    private bool m_FuzzyLogic;
    /// <summary>
    /// (Field) The result of the fuzzy logic calculations
    /// </summary>
    private float m_FuzzyLogicResult;
    /// <summary>
    /// (Field) The maximun value the fazzy logic can take (it might change depending on the factor weights)
    /// </summary>
    private int m_FuzzyLogicResultMax;
    /// <summary>
    /// (Field) The current random factor for the fuzzy logic (it will be calculated onEnable event)
    /// </summary>
    private float m_RandomFactorFuzzyLogic;
    /// <summary>
    /// (Field) The factor of the personality in the fuzzy logic
    /// </summary>
    [SerializeField, Range(0f, 1f), Tooltip ("Coward [0, 0.25), Prudent (0.25, 0.7), Aggresive (0.7, 1]")]
    private float m_CowarAggresiveFactorFLogic;

    /// <summary>
    /// (Field) The weight of the Coward Aggresive Factor int he fuzzy logic
    /// </summary>
    [SerializeField, Range(0f, 1f)]
    private float m_CowardAggresiveWeightFLogic;
    /// <summary>
    /// (Field) The weight of the life in the fuzzy logic
    /// </summary>
    [SerializeField, Range(0f, 1f)]
    private float m_ObjectLifeWeightFLogic;
    /// <summary>
    /// (Field) The weight of the randomFactor in the fuzzy logic
    /// </summary>
    [SerializeField, Range(0f, 1f)]
    private float m_RandomWeightFLogic;

    /// <summary>
    /// (Field) Timer that controls when the fuzzy logic is calculated
    /// </summary>
    private TimerController m_FuzzyLogicTimer;
    /// <summary>
    /// (Field) Seconds that take the Timer to run FuzzyLogic
    /// </summary>
    [SerializeField, Range(0f, 10f)]
    private float m_SecondsToFuzzyLogic;

    /// <summary>
    /// The animal behaviour 
    /// </summary>
    private AIAnimalBehaviour m_AIAnimal;

    ///// <summary>
    ///// (Field) Number of degrees, centred on forward, for the enemy see.
    ///// </summary>
    //[SerializeField, Range(0f, 360f)]
    //private float m_FieldOfViewAngle = 110f;

    /// <summary>
    /// (Field) Whether or not the player is currently sighted.
    /// </summary>
    [SerializeField]
    private bool m_PlayerInSight { get { return ObjectManager.SightController.ObjectInSight; } }

    /// <summary>
    /// (Field) Flag to control if the player was seen at least once
    /// </summary>
    [SerializeField]
    private bool m_PlayerSeen;

    /// <summary>
    /// (Field) The current distance to the player
    /// </summary>
    private float m_DistanceToPlayer;

    /// <summary>
    /// (Field) The radius use to detect a threat
    /// </summary>
    private float m_DangerRadius { get { return ObjectManager.SightController.RadiusOfSight; } }

    /// <summary>
    /// (Field) The vector direction to the player
    /// </summary>
    private Vector3 m_DirectionToPlayer;

    ///// <summary>
    ///// (Field) The angle to the player
    ///// </summary>
    //private float m_AngleToPlayer;

    ///// <summary>
    ///// (Field) The Raycast of the object
    ///// </summary>
    //private RaycastHit m_EnemyRaycast;

    ///// <summary>
    ///// (Field) The point to move
    ///// </summary>
    //private Vector3 m_PointToMove;

    /// <summary>
    /// (Field) Flag controlling if the semi-human is covered
    /// </summary>
    [SerializeField]
    private bool m_CurrentCovered;
    /// <summary>
    /// (Field) The CoverManager of the cover we want to reach
    /// </summary>
    private GameObject m_CoverToGo;
    /// <summary>
    /// (Field) The SafeSpot to go in the cover
    /// </summary>
    private Vector3 m_SafeSpotToGo;

    /// <summary>
    /// (Field) The timer that will control if the object is safe
    /// </summary>
    private TimerController m_ThreatSafeTimer;
    /// <summary>
    /// (Field) The amount of seconds to feel safe after the threat has left
    /// </summary>
    [SerializeField, Range(0f, 20f)]
    private float m_SecondsToFeelSafe;

    /// <summary>
    /// (Field) Timer to control when the enemy needs to recheck for a safeSpot when covered
    /// </summary>
    private TimerController m_ReCheckSafeSpotTimer;
    /// <summary>
    /// (Field) Seconds to recheck for a safeSpot (not to do it every frame)
    /// </summary>
    [SerializeField, Range(0f, 10f)]
    private float m_SecondsToRecheckSafeSpot;

    #endregion

    // Awake is called when the script instance is being loaded
    public void Awake()
    {
        // We add the animal AI to have access to it
        m_AIAnimal = this.gameObject.GetComponent<AIAnimalBehaviour>();

        // We add the timer component
        m_ThreatSafeTimer = ObjectManager.gameObject.AddComponent<TimerController>();
        // We name the label of the timer to identify it
        m_ThreatSafeTimer.ObjectLabel = "SemiHumanThreatTimer";

        // We add the timer component for rechecking the safeSpot
        m_ReCheckSafeSpotTimer = ObjectManager.gameObject.AddComponent<TimerController>();
        // We name the label of this timer to identify in the inspector
        m_ReCheckSafeSpotTimer.ObjectLabel = "SemiHumanReCheckSafeSpotTimer";

        // We add the timer component for slowing down the calculation of FuzzyLogic
        m_FuzzyLogicTimer = ObjectManager.gameObject.AddComponent<TimerController>();
        // We name the label of this timer to identify it in the inspector
        m_FuzzyLogicTimer.ObjectLabel = "SemiHumanFuzzyLogicTimer";

    }

    // This function is called when the object becomes enabled and active
    public void OnEnable()
    {
        // We initialize the component to default values
        Initialize();
    }

    /// <summary>
    /// Semi Human Behaviour, reacting to player's input
    /// </summary>
    public override void Behave()
    {
        // We behave like semiHumans
        SemiHumanLogic();

    }

    /// <summary>
    /// Initializes the component to the default values
    /// </summary>
    private void Initialize()
    {
        // Set player seen to false
        m_PlayerSeen = false;

        // We set current covered to false
        m_CurrentCovered = false;

        // Stop all timers, so that they can reset properly
        m_ThreatSafeTimer.StopTimer();
        m_ReCheckSafeSpotTimer.StopTimer();
        m_FuzzyLogicTimer.StopTimer();

        Debug.Log("SemiHuman Initialized");
    }

    /// <summary>
    /// Contains the logic of the SemiHuman
    /// </summary>
    private void SemiHumanLogic()
    {
        // We update the current distance to the player
        //m_DistanceToPlayer = Vector3.Distance(Toolbox.Instance.GameManager.Player.transform.position,
        //  ObjectManager.ObjectRigidbody.position);
        m_DirectionToPlayer = Vectors.CalculateDirection(ObjectManager.ObjectRigidbody.position,
            Toolbox.Instance.GameManager.Player.ObjectPosition);

        // If the player is inside our DangerRadius...
        if (Vectors.CheckDistance(m_DirectionToPlayer, m_DangerRadius))
        {
            // If the player has never been seen ...
            if (!m_PlayerSeen)
            {
                // We run the animal beaviour one more frame
                m_AIAnimal.Behave();

                // We sight the player ...
                ObjectManager.SightController.Sight(Toolbox.Instance.GameManager.Player.ObjectTransform.position,
                    m_DangerRadius, "Player");
                // If we saw the player... 
                if (m_PlayerInSight)
                {
                    // ... we update the playerSeen flag, now we will not look for him again until we forget we saw him
                    m_PlayerSeen = true;
                    // We calm down the animal to be able to override its behaviour properly
                    m_AIAnimal.CalmAnimal();
                }
            }
            // If the player has been seen at least once (the enemy remembers)... 
            else
            {

                // We make sure that the timer to feel safe is stopped and restarted (so that if the player goes out of radius, we have the same amount of time to wait to feel safe)
                m_ThreatSafeTimer.StopTimer();


                // We update the animator to animate in purple
                ObjectManager.AnimController.Interact();

                // If we have a fuzzy logic...
                if (m_FuzzyLogic)
                {
                    // Every X seconds we run the FuzzyLogic
                    if (m_FuzzyLogicTimer.GenericCountDown(m_SecondsToFuzzyLogic))
                    {
                        // We calculate it
                        FuzzyLogic(); 
                    }
                }

                // We behave depending on the personality active
                switch (m_Personality)
                {
                    case PersonalityEnum.Coward:
                        CowardLogic();
                        break;
                    case PersonalityEnum.Prudent:
                        PrudentLogic();
                        break;
                    case PersonalityEnum.Aggresive:
                        // We completely stop the object                        
                        //ObjectManager.MovementController.Stop();
                        ObjectManager.MovementController.TotalStop();
                        AggressiveLogic();
                        break;
                    default:
                        break;
                }
            }
        }
        // If the player is out of our dangerRadius...
        else
        {
            // If we already saw the player...
            if (m_PlayerSeen == true)
            {
                // We wait for x seconds before feeling safe
                if (m_ThreatSafeTimer.GenericCountDown(m_SecondsToFeelSafe))
                {
                    //Debug.Log("Player out of radius...");
                    // We uncover
                    m_CurrentCovered = false;

                    // We forgot we saw the player
                    m_PlayerSeen = false;                    

                }
            }
            // If we don't remember the player...
            else
            {
                // (It's not needed anymore) We stop the possible control of the rotation 
                //ObjectManager.RotationController.StopControlRotation();
                // We behave like an animal
                m_AIAnimal.Behave();
            }




        }
    }

    /// <summary>
    /// The logic of the coward personality. Will scape from the player like an animal
    /// </summary>
    private void CowardLogic()
    {
        // The object is going to run away, behaving like an animal
        //// We set the direction of danger in the animal AI to the direction to the player
        //m_AIAnimal.DirectionOfDanger = m_DirectionToPlayer.normalized;
        //// We scare the animal
        //m_AIAnimal.AnimalState = AIAnimalBehaviour.AnimalStateEnum.Scared;

        // We scare the animal with the position of the player and update its dangerRadius        
        m_AIAnimal.ScareAnimal(Toolbox.Instance.GameManager.Player.ObjectTransform.position, m_DangerRadius);
       
        // We behave like an animal
        m_AIAnimal.Behave();

        // We completely stop the object
        //ObjectManager.MovementController.TotalStop();
    }

    /// <summary>
    /// The logic of the Prudent personality. Will search for the closest cover to go and shoot, but if there is none, will be coward
    /// </summary>
    private void PrudentLogic()
    {
        // We completely stop the object
        //ObjectManager.MovementController.TotalStop();

        // If the object is not covered...
        if (!m_CurrentCovered)
        {
            // We search the closest cover object
            m_CoverToGo = Vectors.SearchClosestObjectWihTag(ObjectManager.ObjectTransform.position, m_DangerRadius, "Cover");
            // ... If the search returned something...
            if (m_CoverToGo != null)
            {
                // We get the safeSpot that is furthest away from the player
                m_SafeSpotToGo = Points.CalculateFurthestPoint(Toolbox.Instance.GameManager.Player.transform.position, m_CoverToGo.transform.GetComponentsInChildren<Transform>());
                // We go there
                ObjectManager.MovementController.Move(m_SafeSpotToGo);
                // If the object is not moving anymore...
                if (ObjectManager.MovementController.MovementPhase != MovementController.MovementPhaseEnum.Stopped)
                {
                    // The object is now covered
                    m_CurrentCovered = true;
                }
            }
            // If the player did not find a cover...
            else
            {
                // If it didn't, the enemy gets scared and leave
                CowardLogic();
            }


        }
        // If the player is covered already..
        else
        {

            // We wait for x seconds to recheck if we need to change the safeSpot, just if the player has changed position
            if (m_ReCheckSafeSpotTimer.GenericCountDown(m_SecondsToRecheckSafeSpot))
            {
                // We get the safeSpot that is furthest away from the player
                m_SafeSpotToGo = Points.CalculateFurthestPoint(Toolbox.Instance.GameManager.Player.transform.position, m_CoverToGo.transform.GetComponentsInChildren<Transform>());
            }

            // We keep where the closest cover is
            ObjectManager.MovementController.Move(m_SafeSpotToGo);

            // We shoot at the player
            //ObjectManager.WeaponController.Shoot(ObjectManager.WeaponController.WeaponTransform.position, m_DirectionToPlayer.normalized);
            AggressiveLogic();
        }
    }

    /// <summary>
    /// Logic Agressive personality. Will immediately attack the player.
    /// </summary>
    private void AggressiveLogic()
    {
        // We look at the player (we add one unit to avoid shooting at the floor)
        ObjectManager.RotationController.LookAt(Toolbox.Instance.GameManager.Player.ObjectPosition);

        // We shoot at the player
        ObjectManager.WeaponController.Shoot(ObjectManager.WeaponController.WeaponTransform.position, m_DirectionToPlayer.normalized);
    }

    /// <summary>
    /// Selects a personality depending on some input values. See definition for more details
    /// </summary>
    private void FuzzyLogic()
    {                
        // We calculate the maximum value of the fuzzy logic result depending on the weights
        // First, the maximum is zero because we don't know if there is any weight above 0
        m_FuzzyLogicResultMax = 0;
        // If there is any weight on the Coward Aggresive factor...
        if (m_CowardAggresiveWeightFLogic > 0)
        {
            // We add one to the possible result max
            m_FuzzyLogicResultMax += 1;
        }
        // If there is any weight on the life...
        if (m_ObjectLifeWeightFLogic > 0)
        {
            // We add one to the possible result max
            m_FuzzyLogicResultMax += 1;
        }
        // If there is any weight on the Random Factor...
        if (m_RandomWeightFLogic > 0)
        {
            // We add one to the possible result max
            m_FuzzyLogicResultMax += 1;
            // We calculate a random factor for the fuzzy logic
            m_RandomFactorFuzzyLogic = UnityEngine.Random.Range(0f, 1f);
        }

        // Life*Weight + PersonalityFactor*Weight + RandomFactor*Weight = [0, [0,3]]
        m_FuzzyLogicResult = ObjectManager.LifeController.NormalizedLife * m_ObjectLifeWeightFLogic + m_CowarAggresiveFactorFLogic * m_CowardAggresiveWeightFLogic + m_RandomFactorFuzzyLogic * m_RandomWeightFLogic;

        // We normalize the value of the fuzzy logic
        m_FuzzyLogicResult = Normalization.Normalize(m_FuzzyLogicResult, 0f, m_FuzzyLogicResultMax);
        // We select one personality depending on the result of the fuzzy logic
        if (m_FuzzyLogicResult < 0.25f)
        {
            m_Personality = PersonalityEnum.Coward;
        }
        else if (m_FuzzyLogicResult > 0.7f)
        {
            m_Personality = PersonalityEnum.Aggresive;
        }
        else
        {
            m_Personality = PersonalityEnum.Prudent;
        }
    }

    // Implement this OnDrawGizmosSelected if you want to draw gizmos only if the object is selected
    public void OnDrawGizmos()
    {
        if (ObjectManager.AllowGizmos)
        {
            // Yellow for player
            Gizmos.color = Color.white;
            Gizmos.DrawRay(ObjectManager.ObjectRigidbody.position, m_DirectionToPlayer * m_DistanceToPlayer);

            // Blue for forward vector
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(ObjectManager.ObjectRigidbody.position, transform.forward * m_DangerRadius);


        }
    }
}
