﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


[ExecuteInEditMode]
/// <summary>
/// Updates the position of the AIController.PointsToGo only in editor mode.
/// They will be local to the object while in Editor and not in play mode
/// </summary>
[AddComponentMenu("CarlosFramework/PointsController")]
public class PointsController : MonoBehaviour
{
    // We execute the code in unity editor (we also need [ExecuteInEditMode] at the top of the script)
    //#if UNITY_EDITOR
    #region Fields&Properties
    /// The Enemy Manager so that this script has acces to the rest of components
    // Field
    [SerializeField]
    private ObjectManager m_ObjectManager;
    // Property
    public ObjectManager ObjectManager { get { return this.m_ObjectManager; } set { this.m_ObjectManager = value; } }

    /// <summary>
    /// (Field) Flag that controls if the initialPos is saved onAwake()
    /// </summary>
    [SerializeField,Tooltip("Tick to Save the initialPos onAwake()")]
    private bool m_SaveInitialPosition;
    /// <summary>
    /// (Field) The Initial Position of the Object
    /// </summary>
    [SerializeField]
    private Vector3 m_InitialPositon;

    /// <summary>
    /// The field of the array of points
    /// </summary>
    [SerializeField, Tooltip("DON'T MODIFY! Debug Only.")]    
    private Vector3[] m_PointsToGo;
    /// <summary>
    /// The array of points to go
    /// </summary>
    public Vector3[] PointsToGo { get { return this.m_PointsToGo; } set { this.m_PointsToGo = value; } }

    /// <summary>
    /// Aun auxiliary copy of the points to go, to nor modify the original points to go
    /// </summary>
    [SerializeField, Tooltip("Adjust them in Editor time, not in Playmode")]
    private Vector3[] m_EditorPointsToGo;

    [SerializeField, Range(0f, 100f)]
    private float m_RadiusOfPointToCalculate;
    /// <summary>
    /// (Property) The radius that determines the distance to the point to move when we are getting a new point
    ///  For example, if the AIAnimal is scared.
    /// </summary>
    public float RadiusOfPointToCalculate { get { return this.m_RadiusOfPointToCalculate; } set { this.m_RadiusOfPointToCalculate = value; } }

    //[SerializeField]
    //private bool alreadyStarted;

    //[SerializeField]
    //private bool stopControlling; 

    #endregion

    // Awake is called when the script instance is being loaded
    public void Awake()
    {
        // If the objectManager is null...
        if (m_ObjectManager == null)
        {
            // ...We get it from the current gameObject
            m_ObjectManager = GetComponent<ObjectManager>();
        }

        // If we can save the initial position...
        if (m_SaveInitialPosition)
        {
            // We save the Intial position of the Object
            m_InitialPositon = ObjectManager.ObjectTransform.position; 
        }
    }

    // Use this for initialization
    void Start()
    {
        //if (!alreadyStarted && !stopControlling)
        //{
        //    auxPointsToGo = this.PointsToGo; 
        //}
        //alreadyStarted = true;

        
    }

    // This function is called when the object becomes enabled and active
    public void OnEnable()
    {
        // We initialize the component
        Initialize();
        // We run the pointsToGoLogic once
        PointsToGoLogic();        
    }   

    // Update is called once per frame
    void Update()
    {
        //// Only execute while the game is not playing
        //if (!EditorApplication.isPlaying && alreadyStarted && !stopControlling)
        //{

        //    for (int i = 0; i < ObjectManager.AIController.PointsToGo.Length; i++)
        //    {
        //        ObjectManager.AIController.PointsToGo[i] = auxPoints[i] + ObjectManager.transform.position;
        //    }

        //}

        // Only execute while the game is not playing


        //for (int i = 0; i < this.auxPointsToGo.Length; i++)
        //{
        //    this.PointsToGo[i] = auxPointsToGo[i] + ObjectManager.transform.position;
        //}

    }

    //#endif

    /// <summary>
    /// Initializes the component to the default values
    /// </summary>
    private void Initialize()
    {
        // We set the object to its initial position
        ObjectManager.ObjectTransform.position = m_InitialPositon;
    }

    /// <summary>
    /// Converts the array of PointsToGo to the object local space
    /// </summary>
    private void PointsToGoLogic ()
    {
        // We resize the length of pointsToGo if the size is different from auxPointsToGo
        if (PointsToGo.Length != m_EditorPointsToGo.Length)
        {
            // We resize pointsToGo to the auxPointsToGo
            System.Array.Resize<Vector3>(ref m_PointsToGo, m_EditorPointsToGo.Length);
        }

        //System.Array.Copy(auxPointsToGo, PointsToGo, auxPointsToGo.Length);        

        // Every point to go is the actual position of the object plus the offset we want
        for (int i = 0; i < PointsToGo.Length; i++)
        {
            // Every point is local to the object
            PointsToGo[i] = ObjectManager.transform.position + m_EditorPointsToGo[i];
        }
    }    

    // Sent to all game objects before the application is quit
    public void OnApplicationQuit()
    {
        //for (int i = 0; i < ObjectManager.AIController.PointsToGo.Length; i++)
        //{
        //    ObjectManager.AIController.PointsToGo[i] = auxPoints[i];
        //}
        
    }

    /// We draw a gizmo for every pointToGo
    void OnDrawGizmosSelected()
    {

        if (ObjectManager.AllowGizmos)
        {
            // Yellow for all the PointsToGo
            Gizmos.color = Color.yellow;
            for (int i = 0; i < PointsToGo.Length; i++)
            {
                Gizmos.DrawWireSphere(PointsToGo[i], ObjectManager.MovementController.StopRadius);
            }

            // Green for the radius of point to calculate
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(ObjectManager.ObjectRigidbody.position, RadiusOfPointToCalculate);

        }
    }

    
    #if UNITY_EDITOR
    // OnDrawGizmos is executing constantly in the editor
    public void OnDrawGizmos()
    {
        // I am going to write the logic of the editor update here
        // It will not execute in the build, take in mind!

        // We only execute this code if the editor is not in play mode
        if (!EditorApplication.isPlayingOrWillChangePlaymode)
        {
            PointsToGoLogic();

            if (m_SaveInitialPosition)
            {
                m_InitialPositon = ObjectManager.ObjectTransform.position;
            }
        }

    }
    #endif

}
