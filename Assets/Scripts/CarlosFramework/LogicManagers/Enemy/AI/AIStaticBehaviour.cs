﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Static AI, will stop the object totally
/// </summary>
[AddComponentMenu("CarlosFramework/AIStaticBehaviour")]
public class AIStaticBehaviour : AIBehaviour {

    /// The Enemy Manager so that this script has acces to the rest of components
    // Field
    [SerializeField]
    private ObjectManager objectManager;
    // Property
    public ObjectManager ObjectManager { get { return this.objectManager; } set { this.objectManager = value; } }  

    /// <summary>
    /// Static Behaviour, it is not moving
    /// </summary>
    public override void Behave()
    {
        // We completely stop the object
        ObjectManager.MovementController.TotalStop();
    }

    /* 
    Enjoy the cake of nothing! :D
             ,   ,   ,   ,             
           , |_,_|_,_|_,_| ,           
       _,-=|;  |,  |,  |,  |;=-_       
     .-_| , | , | , | , | , |  _-.     
     |:  -|:._|___|___|__.|:=-  :|     
     ||*:  :    .     .    :  |*||     
     || |  | *  |  *  |  * |  | ||     
 _.-=|:*|  |    |     |    |  |*:|=-._ 
-    `._:  | *  |  *  |  * |  :_.'    -
 =_      -=:.___:_____|___.: =-     _= 
    - . _ __ ___  ___  ___ __ _ . -    

    
    */
}
