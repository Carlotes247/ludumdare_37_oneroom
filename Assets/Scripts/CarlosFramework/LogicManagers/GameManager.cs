﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// This script will take charge of managing the game
/// </summary>
[AddComponentMenu("CarlosFramework/GameManager")]
public class GameManager : MonoBehaviour
{
    #region Attributes

    public int coolValue = 100;

    /// Value to allow debug code
    // Field
    [SerializeField]
    private bool allowDebugCode;
    // Property
    public bool AllowDebugCode { get { return this.allowDebugCode; } set { this.allowDebugCode = value; } }

    /// The exposed scoreController so that every other script can interact with it
    // Field
    [SerializeField]
    private ScoreController scoreController;
    // Property
    public ScoreController ScoreController { get { return this.scoreController; } set { this.scoreController = value; } }

    /// The exposed HUD Canvas 
    // Field
    [SerializeField]
    private HUDController hudController;
    // Property
    public HUDController HudController { get { return this.hudController; } set { this.hudController = value; } }

    /// The exposed InputController so that we can control the input from the user
    // Field
    [SerializeField]
    private InputController inputController;
    // Property
    public InputController InputController { get { return this.inputController; } set { this.inputController = value; } }

    // [DEPRECATED] Use now instead Player.WeaponController
    ///// The exposed WeaponController
    //// Field
    //[SerializeField]
    //private WeaponController weaponController;
    //// Property
    //private WeaponController WeaponController { get { return this.weaponController; } set { this.weaponController = value; } }

	[SerializeField, Tooltip("Used in SinglePlayer!")]
    private PlayerManager player;
    /// <summary>
    /// (Property) The main player
    /// </summary>
    public PlayerManager Player { get { return this.player; } }

	[SerializeField, Tooltip ("Only use when you have Multiplayer!")]
	private PlayerManager[] m_Players;
	/// <summary>
	/// The array of players (if there is more than one)
	/// </summary>
	/// <value>The players array</value>
	public PlayerManager[] Players { get { return m_Players; } }


    ///// <summary>
    ///// The game logic controller
    ///// </summary>
    //public GGJ_2016_Logic gameLogicController;

    [SerializeField]
    private CoroutineController coroutineController;
    /// <summary>
    /// (Property) The CoroutineController that provides tools for coroutines
    /// </summary>
    public CoroutineController CoroutineController { get { return this.coroutineController; } set { this.coroutineController = value; } }

    [SerializeField]
    private GameTimeController gameTimeController;
    /// <summary>
    /// (Property) The GameTimeController that controls the timeScale logic
    /// </summary>
    public GameTimeController GameTimeController { get { return this.gameTimeController; } }

    [SerializeField]
    private AudioController audioController;
    /// <summary>
    /// (Property) The AudioController that controls the logic of the audio ingame
    /// </summary>
    public AudioController AudioController { get { return this.audioController; } }

    /// <summary>
    /// (Field) The MenuController that controls the logic of the In-Game Menu
    /// </summary>
    [SerializeField]
    private MenuController m_MenuController;
    /// <summary>
    /// (Property) The MenuController that controls the logic of the In-Game Menu
    /// </summary>
    public MenuController MenuController { get { return this.m_MenuController; } }

    /// <summary>
    /// (Field) The GameLevelController
    /// </summary>
    [SerializeField]
    private GameLevelController m_GameLevelController;
    /// <summary>
    /// (Property) The GameLevelController
    /// </summary>
    public GameLevelController GameLevelController { get { return this.m_GameLevelController; } }

    /// <summary>
    /// (Field) The Main Camera to use in game
    /// </summary>
    [SerializeField]
    private CameraManager m_MainCamera;
    /// <summary>
    /// (Property) The Main Camera to use in game
    /// </summary>
    public CameraManager MainCamera { get { return this.m_MainCamera; } }

    /// <summary>
    /// (Field) The GameLogicController
    /// </summary>
    [SerializeField]
    private GameLogicController m_GameLogicController;
    /// <summary>
    /// (Property) The GameLogicController
    /// </summary>
    public GameLogicController GameLogicController { get { return this.m_GameLogicController; } }

    ///// <summary>
    ///// (Field) Ctrl for Repeatibility experiment
    ///// </summary>
    //[SerializeField]
    //private Repeatibility m_RepeatibilityCtrl;
    ///// <summary>
    ///// (Property) Ctrl for Repeatibility experiment
    ///// </summary>
    //public Repeatibility RepeatibilityCtrl { get { return this.m_RepeatibilityCtrl; } }

    #endregion

    #region MainFunctions

    // Awake is called when the script instance is being loaded
    public void Awake()
    {
        
		if (ScoreController == null) {
			ScoreController = GetComponent<ScoreController>();
		}

		if (HudController == null) {
			HudController = GetComponent<HUDController>();
		} 

		if (m_GameLogicController == null) {
			m_GameLogicController = GetComponent<GameLogicController>();
		}

    }


    #endregion

    public void TestAlive()
    {
        Debug.Log("GameMAnager Alive!");
    }

}
