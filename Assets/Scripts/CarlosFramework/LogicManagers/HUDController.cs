﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------

/*!  This script is ins charge of managing the HUD of the game*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// The controller for the whole HUD. Right now, it controls the InGameCursor and the ScoreText
/// </summary>
[AddComponentMenu("CarlosFramework/HUDController")]
public class HUDController : MonoBehaviour
{

    #region Attributes
    /// The text of the score
    // Field
    [SerializeField]
    private Text scoreText;
    // Property
    public Text ScoreText { get { return this.scoreText; } set { this.scoreText = value; } }

    /// The inGame cursor to move
    // Field
    [SerializeField]
    private Image inGameCursor;
    // Property
    public Image InGameCursor { get { return this.inGameCursor; } set { this.inGameCursor = value; } }

    [SerializeField]
    private Text storyText;
    /// <summary>
    /// (Property) The ingame story text to show on the hud
    /// </summary>
    public Text StoryText { get { return this.storyText; } set { this.storyText = value; } }

    /// <summary>
    /// (Field) The ammo text in the HUD
    /// </summary>
    [SerializeField]
    private Text m_AmmoText;
    /// <summary>
    /// (Property) The ammo text in the HUD
    /// </summary>
    public Text AmmoText { get { return this.m_AmmoText; } set { this.m_AmmoText = value; } }

    /// <summary>
    /// (Field) The ammo reload bar in the HUD
    /// </summary>
    [SerializeField]
    private GameObject m_AmmoReloadBar;
    /// <summary>
    /// (Field) The instantiated progress of the ammo reload bar
    /// </summary>
    private Vector3 m_AmmoReloadBarProgress;

    /// <summary>
    /// The class containing the details of an UI Panel with Text inside
    /// </summary>
    [System.Serializable]
    public class PanelText
    {
        /// <summary>
        /// Panel to (de)activate to show UI
        /// </summary>
        [SerializeField]
        private GameObject m_PanelGObject;
        /// <summary>
        /// Text that shows desired state
        /// </summary>
        [SerializeField]
        private Text m_InsideText;

        /// <summary>
        /// Shows the Panel Text
        /// </summary>
        /// <param name="textToShow">The text to update inside the panel</param>
        /// <param name="textColor">The color of the text (optional)</param>
        public void ShowUI(string textToShow, Color? textColor = null)
        {
            // We activate the panel
            m_PanelGObject.SetActive(true);
            // Write the text inside
            m_InsideText.text = textToShow;
            // Change the color if passed in
            if (textColor != null)
            {
                m_InsideText.color = (Color)textColor;
            }
        }

        /// <summary>
        /// Hides the Panel text
        /// </summary>
        public void HideUI()
        {
            m_PanelGObject.SetActive(false);
        }

    }
    /// <summary>
    /// The UI that shows when a level ends
    /// </summary>
    [SerializeField]
	private PanelText m_EndLevelUI;
    /// <summary>
    /// (Property) The UI that shows when a level ends
    /// </summary>
    public PanelText EndLevelUI { get { return this.m_EndLevelUI; } }

	/// <summary>
	/// The UI that shows when the game ends
	/// </summary>
	[SerializeField]
	private PanelText m_EndGameUI;
	public PanelText EndGameUI { get { return m_EndGameUI;} }



    #endregion   

    /// The function to update the score on the text
    public void UpdateHUDScore(string textToWrite)
    {
        ScoreText.text = textToWrite;
    }

    /// <summary>
    /// Updates the content of the StoryText in HUD
    /// </summary>
    /// <param name="textToWrite"> The string containing the text to show on the HUD </param>
    public void UpdateStoryText(string textToWrite)
    {
		if (StoryText != null) {
			StoryText.text = textToWrite;
		} else {
			//Debug.LogError ("HudController.StoryText is null");
		}

    }

    public void TestAlive()
    {
        Debug.Log("HUDScoreAlive!");
    }

    public void DebugHUDScore ()
    {
        if (Toolbox.Instance.GameManager.AllowDebugCode)
        {
            Debug.Log("Score on HUD: " + ScoreText.text);
        }
    }

    /// <summary>
    /// Updates the text in a UIText
    /// </summary>
    /// <param name="textObjective"> The UI Text to update</param>
    /// <param name="textToWrite"> The string to write there</param>
    public void UpdateUIText (Text textObjective, string textToWrite)
    {
        textObjective.text = textToWrite;
    }

    /// <summary>
    /// Public method to update the ammo reload bar
    /// </summary>
    /// <param name="progress"> Progress to set. 0 empty, 1 full.</param>
    public void UpdateAmmoReloadBar (float progress)
    {
        // We update the local copy of the progress
        m_AmmoReloadBarProgress = m_AmmoReloadBar.transform.localScale;
        // We set the x value to the progress passed in
        m_AmmoReloadBarProgress.x = progress;
        // We set it to the ammo reload bar in the HUD
        m_AmmoReloadBar.transform.localScale = m_AmmoReloadBarProgress;
    }
}
