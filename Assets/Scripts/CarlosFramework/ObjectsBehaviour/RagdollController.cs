﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using ReusableMethods;
using System;

/// <summary>
/// The controller of the Ragdoll
/// </summary>
[AddComponentMenu("CarlosFramework/RagdollController")]
public class RagdollController : MonoBehaviour
{

    /// <summary>
    /// (Field) The ragdoll to control
    /// </summary>
    [SerializeField]
    private GameObject m_Ragdoll;

    /// <summary>
    /// (Field) The array of different children gameObject in the Ragdoll
    /// </summary>
    [SerializeField]
    private Transform[] m_RagdollParts;
    /// <summary>
    /// (Field) The array of different children character joints in the ragdoll
    /// </summary>
    [SerializeField]
    private GameObject[] m_RagdollCharacterJoints;

    [SerializeField]
    private GameObject[] m_OriginalValuesRagdollParts;
    [SerializeField]
    private CharacterJoint[] m_OriginalValuesRagdollCharacterJoints;
    [SerializeField]
    private CharacterJoint m_AuxCharacterJoint;

    // Awake is called when the script instance is being loaded
    public void Awake()
    {
        FindAllRagdollParts();
        SaveOriginalRagdollParts();
    }

    // This function is called when the object becomes enabled and active
    public void OnEnable()
    {
        LoadOriginalRagdollParts();
    }

    // This function is called when the behaviour becomes disabled or inactive
    public void OnDisable()
    {
        
    }

    /// <summary>
    /// Finds all the parts of the ragdoll as children
    /// </summary>
    private void FindAllRagdollParts()
    {
        // We populate the RagdollParts 
        Arrays.FindComponentAsChildren<Transform>(ref m_RagdollParts, m_Ragdoll);

        //// We find all the character joints
        //Arrays.FindComponentAsChildren<CharacterJoint>(ref m_RagdollCharacterJoints, m_Ragdoll);
    }

    /// <summary>
    /// Saves the original values of all the parts
    /// </summary>
    private void SaveOriginalRagdollParts()
    {
        // We resize the original Arrays
        Array.Resize<GameObject>(ref m_OriginalValuesRagdollParts, m_RagdollParts.Length);        
        Array.Resize<CharacterJoint>(ref m_OriginalValuesRagdollCharacterJoints, m_RagdollCharacterJoints.Length);

        // We loop through all the arrays and save them into the original
        // Ragdoll parts
        for (int i = 0; i < m_RagdollParts.Length; i++)
        {
            // We instantiate a GameObject in each Array position
            m_OriginalValuesRagdollParts[i] = new GameObject();
            // We copy in the new GameObject's transform the position and rotation from the ragdoll's parts
            m_OriginalValuesRagdollParts[i].transform.position = m_RagdollParts[i].position;
            m_OriginalValuesRagdollParts[i].transform.rotation = m_RagdollParts[i].rotation;
            // We make all the new GameObjects children of the current object
            m_OriginalValuesRagdollParts[i].transform.parent = this.gameObject.transform;
        }
        //// CharachterJoints
        //for (int i = 0; i < m_RagdollCharacterJoints.Length; i++)
        //{
        //    m_OriginalValuesRagdollCharacterJoints[i] = m_RagdollCharacterJoints[i].GetComponent<CharacterJoint>();
        //}
    }

    /// <summary>
    /// Loads the original ragdoll parts
    /// </summary>
    private void LoadOriginalRagdollParts()
    {
        // We loop through all the arrays and save them into the original
        // Ragdoll parts
        for (int i = 0; i < m_RagdollParts.Length; i++)
        {
            m_RagdollParts[i].position = m_OriginalValuesRagdollParts[i].transform.position;
            m_RagdollParts[i].rotation = m_OriginalValuesRagdollParts[i].transform.rotation;
        }
        //// CharachterJoints
        //for (int i = 0; i < m_RagdollCharacterJoints.Length; i++)
        //{
        //    m_RagdollCharacterJoints[i].GetComponent<CharacterJoint>() = m_OriginalValuesRagdollCharacterJoints[i];
        //}
    }

}
