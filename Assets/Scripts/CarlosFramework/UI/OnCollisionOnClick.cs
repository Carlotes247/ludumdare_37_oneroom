﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// This component, once attached to a button, let it be triggerred by collisions (right now only bullets)
/// </summary>
public class OnCollisionOnClick : MonoBehaviour
{

    /// The button to click when a bullet collides with it
    [SerializeField]
    private Button buttonToClick;

    // Awake is called when the script instance is being loaded
    public void Awake()
    {
        buttonToClick = this.gameObject.GetComponent<Button>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            buttonToClick.image.color = buttonToClick.colors.pressedColor;
            buttonToClick.onClick.Invoke();
            if (Toolbox.Instance.GameManager.AllowDebugCode)
            {
                Debug.Log("I've been shot OnCollisionEnter!");

            }
        }
        //this.gameObject.SendMessage("OnClick");
    }

    // OnTriggerEnter is called when the Collider other enters the trigger
    public void OnTriggerEnter(Collider other)
    {       
        if (other.CompareTag("Bullet"))
        {
            buttonToClick.image.color = buttonToClick.colors.pressedColor;
            buttonToClick.onClick.Invoke();
            if (Toolbox.Instance.GameManager.AllowDebugCode)
            {
                Debug.Log("I've been shot OnTriggerEnter!");

            }            
        }
        //gameObject.SendMessage("OnClick");
        //other.gameObject.GetComponent<Button>().onClick.Invoke();

    }


}
