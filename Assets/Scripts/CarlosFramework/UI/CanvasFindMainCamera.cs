﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------

/*! \file CanvasFindMainCamera.cs
 *  \brief This script finds the main camera and assign it to the canvas to draw in world space.
 * */
using UnityEngine;
using System.Collections;

public class CanvasFindMainCamera : MonoBehaviour {

    public Canvas canvas;
    private bool cameraFound;

	// Use this for initialization
	void Start () {
        canvas.renderMode = RenderMode.WorldSpace;
        cameraFound = false;
	}
	
	// Update is called once per frame
	void Update () {
        FindMainCamera();
	}

    // The function for finding the camera
    void FindMainCamera() {
        if (canvas.worldCamera == null)
        {
            Debug.Log("No main camera found");
            canvas.worldCamera = Camera.main;
            cameraFound = true;
        }
        //else
        //{
        //    //Debug.Log("Main camera found!");
        //    //canvas.transform.position = Camera.main.transform.position;
        //    //canvas.transform.position += Vector3.forward * 20;
        //}
    }
}
