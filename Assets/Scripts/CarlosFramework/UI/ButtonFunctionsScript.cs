//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script will store a lot of functions
/// </summary>
public class ButtonFunctionsScript : MonoBehaviour {

	/// The result controller of the scene (if any)
	GameObject resultController;
	

	/// Start is called just before any of the
	/// Update methods is called the first time.
	void Start () {
		//FindResulController();
	}
	
	/// Update is called every frame, if the
	/// MonoBehaviour is enabled.
	void Update () {
		
	}

	/// Loads a level depending on a name
	public void LoadLevel (string levelName) {
		Application.LoadLevel(levelName);
	}

	/// Exits the game
	public void ExitGame () {
		Application.Quit();
	}

	/// This function communicates with result controller and set the minigame to pass
	public void PassMinigame () {
		resultController.SendMessage("PassMinigame");
	}

	/// This function communicates with result controller and set the minigame to fail
	public void FailMinigame () {
		resultController.SendMessage("FailMinigame");
	}

	/// Finds the result controller in the scene
	void FindResulController () {
		GameObject aux = GameObject.FindWithTag("ResultController");
		// We check if aux is null. If it is, there is no ResultController in the scene
		if (aux!= null) {
			this.resultController = aux;
		}
	}
	
}
