﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class SticksNRoses_GameLogic : GameLogicController {

	/// <summary>
	/// The index of the level to load first
	/// </summary>
	[SerializeField, Tooltip("Check Build Settings for level 1")]
	private int m_LevelToLoadFirst;

	/// <summary>
	/// (Field) True if the game is lost
	/// </summary>
	private bool m_LooseFlag;
	/// <summary>
	/// (Property) True if the game is lost
	/// </summary>
	public override bool LooseFlag { get { return this.m_LooseFlag; } }

	/// <summary>
	/// (Field) True if the game is won
	/// </summary>
	private bool m_WinFlag;
	/// <summary>
	/// (Property) True if the game is won
	/// </summary>
	public override bool WinFlag { get { return this.m_WinFlag; } }

	/// <summary>
	/// (Field) Array containing all the players
	/// </summary>
	[SerializeField]
	private GameObject[] m_Players;
    /// <summary>
    /// (Property) Array containing all the players
    /// </summary>
    public GameObject[] Players { get { return m_Players; } }

	/// <summary>
	/// (Field) Array containing materials for players
	/// </summary>
	[SerializeField]
	private Material[] materials;
    /// <summary>
    /// (Property) Array containing materials for players
    /// </summary>
    public Material[] Materials { get { return materials; } }

	/// <summary>
	/// The material for the goal
	/// </summary>
	[SerializeField]
	private Material m_GoalMaterial;

	/// <summary>
	/// (Field) Array containing materials for players
	/// </summary>
	[SerializeField]
	private Renderer[] renderers;

	/// <summary>
	/// The timer that controls the player switch
	/// </summary>
	private TimerController m_SwitchTimerCtrl;
	/// <summary>
	/// The amount of time to run the timer
	/// </summary>
	[SerializeField]
	private Vector2 m_SwitchTimerBoundaries;
	/// <summary>
	/// The seconds the timer inputs
	/// </summary>
	[SerializeField]
	private float m_SecondSwitchTimer;

	/// <summary>
	/// How many seconds before switch occurs should we start showing SWITCH flashes?
	/// </summary>
	[SerializeField]
	private float timeFlashScreen;
	private float switchAlpha;
	private Image switchFlashImage;
	private bool switchFlashUp;
	public GameObject alarmSound;

	/// <summary>
	/// object and coroutine for showing the SWITCH! text
	/// </summary>
    [SerializeField]
	private GameObject switchText;
	private IEnumerator coroutineSwitchText;

	// The temp material to switch colors
	private Material temp;

	/// <summary>
	/// The timer to end the level
	/// </summary>
	private TimerController m_EndLevelTimer;
	/// <summary>
	/// The seconds to end the level
	/// </summary>
	[SerializeField]
	private float m_SecondsToEndLevel;

	/// <summary>
	/// The color of the Winner
	/// </summary>
	private Color m_WinnerColor;
	private PlayerRoleCtrl.PlayerColorsEnum m_WinnerColorID;

	/// <summary>
	/// Timer to hide the winner message
	/// </summary>
	private TimerController m_HideWinnerMessageTimer;
	/// <summary>
	/// The seconds to show winner message.
	/// </summary>
	[SerializeField, Range (0, 5)]
	private float m_SecondsToShowWinnerMessage;

	// Use this for initialization
	void Start () {
		// We initalize the game
		Initialize();
        // find switch message object and deactivate it
        if (switchText == null)
        {
            switchText = GameObject.Find("switch_text");
        }
        switchText.SetActive(false);

		// We start the game!
		//StartGame ();

		// We load the Main Menu Scene, which in this game is the level after the last GameLevel
		//Toolbox.Instance.GameManager.GameLevelController.LoadScene(Toolbox.Instance.GameManager.GameLevelController.GameLevels.Length);


	}

	// Update is called once per frame
	void Update () {
		// We countdown to the Switch

		// if there is less than timeFlashScreen seconds before switch the screen flashes (only if the menu is closed and the game/level is not won)
		if (m_SwitchTimerCtrl.SecondsLeft < timeFlashScreen && !Toolbox.Instance.GameManager.MenuController.MenuOpen 
            && !Toolbox.Instance.GameManager.GameLevelController.GameLevels[Toolbox.Instance.GameManager.GameLevelController.IndexCurrentLevel].GetComponent<GameLevelLogic>().LevelWinFlag) {
			alarmSound.SetActive (true);
			if (switchFlashUp) {
				switchAlpha += 0.1f;
				if (switchAlpha > 0.9) {
					switchFlashUp = false;
				}
			} else {
				switchAlpha -= 0.1f;
				if (switchAlpha < 0.1) {
					switchFlashUp = true;
				}
			}
			switchFlashImage.color = new Color (1, 0, 0, switchAlpha);
		} else {
			alarmSound.SetActive (false);
			switchFlashImage.color = new Color (1, 0, 0, 0);
		}

		// If the menu is open...
		if (Toolbox.Instance.GameManager.MenuController.MenuOpen) {
			// Pause Timers
			m_SwitchTimerCtrl.PauseTimer();
			m_EndLevelTimer.PauseTimer();
			m_HideWinnerMessageTimer.PauseTimer ();
		} else {
			// Resume timers
			m_SwitchTimerCtrl.ResumeTimer();
			m_EndLevelTimer.ResumeTimer();
			m_HideWinnerMessageTimer.ResumeTimer ();
		}

		if (m_SwitchTimerCtrl.GenericCountDown(m_SecondSwitchTimer)) {
			// Show switch message
			//showSwitchMessage();
			switchText.SetActive(true);
			StartCoroutine(WaitBeforeDeactivatingSwitchText());
			Debug.Log ("SWITCH PLAYERS");
			// We randomize again the seconds to wait fir the next swicth
			RandomizeSwitchTimer();
            // switch materials (colors) of the players
            //changePlayerColors();
            SwitchPlayerRoles();
			// reset switch flashing alpha
			switchAlpha = 0;
			switchFlashImage.color = new Color (1, 0, 0, 0);
		}

		// If we haven't either won or lost...
		if (m_WinFlag == m_LooseFlag) {

			// Countdown to end the level
			if (m_EndLevelTimer.GenericCountDown(m_SecondsToEndLevel)) {

				// THIS IS NOW CALLED INSIDE GAME LEVEL LOGIC WHEN WE CALL  WinLevel()
				//				// The player that is the ball wins (2nd position in the players array will always be the ball)
				//				Win( m_Players[1].GetComponent<PlayerManager>().PlayerRoleCtrl.CurrentControlID );

				/* We are now executing the win level logic inside here, it should be in a separate ctrl, but is fine */

				// We get the ID of the player that won the level being the ball
				int playerIDThatWonLevel = m_Players [1].GetComponent<PlayerManager> ().PlayerRoleCtrl.CurrentControlID;

				// We get the current level index
				int currentLevel = Toolbox.Instance.GameManager.GameLevelController.IndexCurrentLevel;

				// We set the player that won that level inside the gamelevellogic ctrl
				Toolbox.Instance.GameManager.GameLevelController.GameLevels[currentLevel].GetComponent<GameLevelLogic>().PlayerThatWonLevel = playerIDThatWonLevel;

				// We win that level for that player
				Toolbox.Instance.GameManager.GameLevelController.GameLevels[currentLevel].GetComponent<GameLevelLogic>().WinLevel();


			}

			// Update HUD timer without any decimals [ToString("F0")]
			Toolbox.Instance.GameManager.HudController.UpdateHUDScore(m_EndLevelTimer.SecondsLeft.ToString("F0"));
		}

	}

	/// <summary>
	/// Raises the application quit event.
	/// </summary>
	void OnApplicationQuit() {
		// restart player colors
		RestartPlayerColors ();
	}

	/// <summary>
	/// Initializes the component to the default values
	/// </summary>
	public void Initialize ()
	{
		// We set all the flags to false
		m_LooseFlag = false;
		m_WinFlag = false;

		// Add a timer component
		m_SwitchTimerCtrl = this.gameObject.AddComponent<TimerController>();
		// Label it
		m_SwitchTimerCtrl.ObjectLabel = "Switch Player Timer";

		// init switch flash thingies
		switchAlpha = 0;
		switchFlashImage = GameObject.Find ("SwitchFlash").GetComponent<Image>();
		switchFlashImage.color = new Color (1, 0, 0, switchAlpha);
		switchFlashUp = true;

		// init alarm sound to false
		alarmSound.SetActive(false);

		// Randomize seconds to wait
		RandomizeSwitchTimer();

		// Add the end level timer ctrl
		m_EndLevelTimer = this.gameObject.AddComponent<TimerController>();
		m_EndLevelTimer.ObjectLabel = "End Level Timer";

		// Add the hide End Game message timer
		m_HideWinnerMessageTimer = this.gameObject.AddComponent<TimerController>();
		m_HideWinnerMessageTimer.ObjectLabel = "Hide Winner Message Timer";

        // Hide the EndGame Text
        Toolbox.Instance.GameManager.HudController.EndLevelUI.HideUI();

		// We assign player IDs and colors
		for (int i = 0; i < m_Players.Length; i++) {
			m_Players[i].GetComponent<PlayerManager> ().PlayerRoleCtrl.PlayerID = i;
			// The color is already specified inside the component
			m_Players [i].GetComponent<PlayerManager> ().PlayerRoleCtrl.ApplyPlayerColorID ();
		}

		// We update the material of the goal with the same color as the stick figure (P0)
		m_GoalMaterial.color = materials[0].color;

        // We assign the player roles (P0 is stickFigure, P1 is ball) at the beginning
        m_Players[0].GetComponent<PlayerManager>().PlayerRoleCtrl.SetPlayerRole(PlayerRoleCtrl.PlayerRolesEnum.StickFigure);
        m_Players[1].GetComponent<PlayerManager>().PlayerRoleCtrl.SetPlayerRole(PlayerRoleCtrl.PlayerRolesEnum.Ball);

        // We open the mainMenu
        Toolbox.Instance.GameManager.MenuController.OpenMainMenu(true);

    }

	/// <summary>
	/// Saves the state of the original game.
	/// </summary>
	public void RestartGame () {
		// We set all the flags to false
		m_LooseFlag = false;
		m_WinFlag = false;

		// We set up the start values for the SwitchFlash
		switchFlashImage.color = new Color (1, 0, 0, switchAlpha);
		switchFlashUp = true;

		// Hide the EndLevel Text
		Toolbox.Instance.GameManager.HudController.EndLevelUI.HideUI();
		// Hide the EndGame Text
		Toolbox.Instance.GameManager.HudController.EndGameUI.HideUI();

		// We make sure all the timers are stopped
		m_EndLevelTimer.StopTimer();
		m_HideWinnerMessageTimer.StopTimer ();
		m_SwitchTimerCtrl.StopTimer ();

		// We restart the color of the players
		RestartPlayerColors();

		// We restart the levels
		RestartLevels();

		// We restart the dash of the ball
		m_Players[1].GetComponent<PlayerManager>().MovementController.DashCtrl.ResetDashToMax();


	}

    /// <summary>
    /// IEnumerator with a counter to show switch colors. Instantiate in a coroutine
    /// </summary>
    /// <returns></returns>
	private IEnumerator WaitBeforeDeactivatingSwitchText() {
		while (true) {
			yield return new WaitForSeconds(0.5f);
			switchText.SetActive(false);
		}
	}

//	private IEnumerator showSwitchMessage() {		
//		Debug.Log("teeeest");
//		switchText = GameObject.Find("switch_text");
//		Debug.Log(switchText);
//		switchText.SetActive(true);
		// will wait for 0.5s before deactivating switch text
//		StartCoroutine(coroutineSwitchText);
//	}

    /// <summary>
    /// Change player colors 
    /// </summary>
	private void changePlayerColors() {
		Material temp;

		renderers[0] = m_Players[0].GetComponent<Renderer>();
		renderers [1] = m_Players[1].GetComponent<Renderer>();
		renderers [0].material = materials [1];
		renderers [1].material = materials [0];
		temp = materials [0];
		materials [0] = materials [1];
		materials [1] = temp;
	}

    /// <summary>
    /// Handles the Switching roles of the players
    /// </summary>
    private void SwitchPlayerRoles()
    {
        // For all the players, switch roles
        for (int i = 0; i < m_Players.Length; i++)
        {
            m_Players[i].GetComponent<PlayerManager>().PlayerRoleCtrl.SwitchPlayerRole();
        }

		// We update the material of the goal with the same color as the stick figure (P0)
		m_GoalMaterial.color = materials[0].color;
    }

	/// <summary>
	/// Loose then game when called
	/// </summary>
	public override void Loose()
	{
		if (!m_LooseFlag) {
			// We set the loose flag to true
			m_LooseFlag = true;

            // Show the EndGameUI and update text
            Toolbox.Instance.GameManager.HudController.EndLevelUI.ShowUI("Red Player Wins!", Color.red);

            // We make sure that the timer stops
            m_EndLevelTimer.StopTimer();

		}
	}

	/// <summary>
	/// [NOT IMPLEMENTED] Wins then game when called
	/// </summary>
	public override void Win ()
	{
		throw new System.NotImplementedException ();
	}

	/// <summary>
	/// Wins the game when called, specifying which player
	/// </summary>
	/// <param name="playerID">the ID of the player that won</param>
	public override void Win(int playerID)
	{
		if (!m_WinFlag)
		{
			// If all the levels have been completed...
			if (Array.TrueForAll(Toolbox.Instance.GameManager.GameLevelController.GameLevels, GameLevelLogic.CheckLevelCompleted))
			{
				// We set the win flag to true
				m_WinFlag = true;
				Debug.Log("Game Won");
				Toolbox.Instance.GameManager.HudController.UpdateStoryText("Game Won"); 

				// We perform the end Game logic
				WinLogic();
				
			}
			// If there are levels left to complete...
			else
			{
				Debug.Log("There are still levels left to complete!");
				Toolbox.Instance.GameManager.HudController.UpdateStoryText("There are still levels left to complete!");
			}
				
			// end/win the level
			int currentLevel = Toolbox.Instance.GameManager.GameLevelController.IndexCurrentLevel;
			Debug.Log("win GAME called, curentlevelindex is: " + currentLevel);
			//Toolbox.Instance.GameManager.GameLevelController.GameLevels[currentLevel].GetComponent<GameLevelLogic>().WinLevel();
			
			// Shows the logic of showing the winning message
			ShowWinnerLevelMessageLogic(playerID);

        }
    }

	/// <summary>
	/// Starts the game when called
	/// </summary>
	public override void StartGame()
	{
		// We don't execute if the GameManager is not instantiated
		if (Toolbox.Instance.GameManager == null) {
			return;
		}

		if (Toolbox.Instance.GameManager.MenuController != null) {
			// We close the Main Menu
			Toolbox.Instance.GameManager.MenuController.OpenMainMenu(false);
		}

		if (Toolbox.Instance.GameManager.GameLevelController != null) {
			// We load the first level
			Toolbox.Instance.GameManager.GameLevelController.LoadGameLevel(m_LevelToLoadFirst);
		}

		// We place the player in the initial position [DEPRECATED- Now it is responsibality of the GameLevelLogic]
		//Toolbox.Instance.GameManager.Player.MovementController.Teleport(m_InitialPlayerPosition);
	}

	/// <summary>
	/// Sets the player in the initial position
	/// </summary>
	/// <param name="pos"> The position to set the player</param>
	public override void SetPlayerAtInitialPosition(Vector3 pos)
	{
		// We teleport the player to the position passed in
		Toolbox.Instance.GameManager.Player.MovementController.Teleport(pos);

		// We make sure that no residual rigidbody speed is in the player
		Toolbox.Instance.GameManager.Player.ObjectRigidbody.velocity = Vector3.zero;
		Toolbox.Instance.GameManager.Player.ObjectRigidbody.angularVelocity = Vector3.zero;
		// We reset the rotation of the player
		Toolbox.Instance.GameManager.Player.RotationController.ResetRotation();

	}

	/// <summary>
	/// Sets the players at initial position.
	/// </summary>
	/// <param name="posArray">Position array.</param>
	public override void SetPlayersAtInitialPosition(Vector3[] posArray) {
		for (int i = 0; i < posArray.Length; i++) {
			// We teleport the player to the position passed in
			Toolbox.Instance.GameManager.Players[i].MovementController.Teleport(posArray[i]);

			// We make sure that no residual rigidbody speed is in the player
			Toolbox.Instance.GameManager.Players[i].ObjectRigidbody.velocity = Vector3.zero;
			Toolbox.Instance.GameManager.Players[i].ObjectRigidbody.angularVelocity = Vector3.zero;
			// We reset the rotation of the player
			if (Toolbox.Instance.GameManager.Players[i].RotationController != null) {
				Toolbox.Instance.GameManager.Players[i].RotationController.ResetRotation();
			}
		}
	}

	/// <summary>
	/// Randomizes the seconds to wait for the switch
	/// </summary>
	private void RandomizeSwitchTimer() {
		// min and max are set from the boundaries
		m_SecondSwitchTimer = Random.Range (m_SwitchTimerBoundaries.x, m_SwitchTimerBoundaries.y);
	}

	/// <summary>
	/// Shows the winner message logic.
	/// </summary>
	/// <param name="playerID">Player ID.</param>
	public void ShowWinnerLevelMessageLogic(int playerID) {
		// We access the PlayerRoleCtrl of the winner and get its color
		m_WinnerColor = m_Players[playerID].GetComponent<PlayerManager>().PlayerRoleCtrl.PlayerColor;
		// We get the color id as well
		m_WinnerColorID = m_Players[playerID].GetComponent<PlayerManager>().PlayerRoleCtrl.PlayerColorID;

		// We show the EndGameUI and update Text getting the color out from the enum
		Toolbox.Instance.GameManager.HudController.EndLevelUI.ShowUI( m_WinnerColorID.ToString(), m_WinnerColor);

		// We make sure that the timer stops
		m_EndLevelTimer.StopTimer();

		// We start the coroutine that will hide the winning message
		Toolbox.Instance.GameManager.CoroutineController.StartCoroutine (HideWinnerMessage (m_SecondsToShowWinnerMessage));


	}

	/// <summary>
	/// Shows the winner game message, with option to modifying the color text with extraText.
	/// </summary>
	/// <param name="playerID">Player ID.</param>
	/// <param name="extraText">Extra text to override the Color Text. It will be shown in black.</param>
	public void ShowWinnerGameMessageLogic(int playerID, bool? isDraw=null) {
		// We access the PlayerRoleCtrl of the winner and get its color
		m_WinnerColor = m_Players[playerID].GetComponent<PlayerManager>().PlayerRoleCtrl.PlayerColor;
		// We get the color id as well
		m_WinnerColorID = m_Players[playerID].GetComponent<PlayerManager>().PlayerRoleCtrl.PlayerColorID;

		if (isDraw == null) {
			// We show the EndGameUI and update Text getting the color out from the enum
			Toolbox.Instance.GameManager.HudController.EndGameUI.ShowUI( m_WinnerColorID.ToString(), m_WinnerColor);
		} else {
			// We show the EndGameUI and update Text showing the override ColorText in black color
			Toolbox.Instance.GameManager.HudController.EndGameUI.ShowUI( "NOBODY ", Color.black);
		}



		// We make sure that the timer stops
		m_EndLevelTimer.StopTimer();
	}

	/// <summary>
	/// Hides the winner message.
	/// </summary>
	/// <returns>The coroutine to instantiate.</returns>
	/// <param name="secondsToHide">Seconds to hide.</param>
	private IEnumerator HideWinnerMessage (float secondsToHide) {
		
		while (true) {
			// We wait for X seconds...
			if (m_HideWinnerMessageTimer.GenericCountDown(secondsToHide)) {

				// We close the winner message
				Toolbox.Instance.GameManager.HudController.EndLevelUI.HideUI();

				// We exit the coroutine
				yield break;

			}

			// We wait one frame
			yield return null;
		}
	}

	/// <summary>
	/// Resets the level timer
	/// </summary>
	public void ResetLevelTimer () {
		m_EndLevelTimer.StopTimer ();
	}

	/// <summary>
	/// All the logic to perform when we win the game
	/// </summary>
	private void WinLogic() {
	
		// We select the winner
		int p0LevelsWon = 0;
		int p1LevelsWon = 0;
		// We start at 1 the for loop to skip the first level (the main menu)
		for (int i = 1; i < Toolbox.Instance.GameManager.GameLevelController.GameLevels.Length; i++) {
			// We see who won that level
			switch (Toolbox.Instance.GameManager.GameLevelController.GameLevels [i].GetComponent<GameLevelLogic> ().PlayerThatWonLevel) {
			case 0: 
				// We add one level won to P0!
				p0LevelsWon++;
				break;
			case 1:
				// We add one level won to P1!
				p1LevelsWon++;
				break;
			default:
				// We debug an error
				Debug.LogError("Wrong PlayerID in level: " + i);
				break;
			} 

		}

		// We select the winner based on who has more levels won
		if (p0LevelsWon > p1LevelsWon) {
			// We show the end screen for P0
			ShowWinnerGameMessageLogic (0);
		} else if (p0LevelsWon < p1LevelsWon) {
			// We show the end screen for P1
			ShowWinnerGameMessageLogic (1);
		} else {
			// They are equal! Say it in the end screen!
			ShowWinnerGameMessageLogic (0, true);
		}
			

	}

	/// <summary>
	/// Restarts the player colors.
	/// </summary>
	private void RestartPlayerColors() {
		// We restart player colors when we quit, to avoid having different materials all the time
		for (int i = 0; i < m_Players.Length; i++) {
			// The color is already specified inside the component
			m_Players [i].GetComponent<PlayerManager> ().PlayerRoleCtrl.RestartPlayerColor ();
		}

		// We update the material of the goal with the same color as the stick figure (P0)
		m_GoalMaterial.color = materials[0].color;
	}

	/// <summary>
	/// Restarts the levels.
	/// </summary>
	private void RestartLevels() {
		// We go through the levels. We start in 1 because 0 is the main menu
		for (int i = 1; i < Toolbox.Instance.GameManager.GameLevelController.GameLevels.Length; i++) {
			// We restart our levels
			Toolbox.Instance.GameManager.GameLevelController.GameLevels [i].GetComponent<GameLevelLogic> ().RestartLevelState ();
		}
	}

}
