﻿#if !UNITY_WEBGL
#define NON_WEBGL_BUILD
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationRoomCtrl : MonoBehaviour {

    #region Attributes

    [SerializeField]
    LD_LogicCtrl m_GameLogic;

    [SerializeField]
    GameObject m_PlayerInLevel;
    
    [SerializeField]
    Vector3 m_InitialPositionOfPlayer;

    /// <summary>
    /// The token of the inital position for the player
    /// </summary>
    [SerializeField]
    Transform m_InitialPosToken;

    /// <summary>
    /// Array of buttons that needs to be pressed
    /// </summary>
    [SerializeField]
    private bool[] m_ButtonsPressed;
    [SerializeField]
    private float[] m_TriggersPressed;
    /// <summary>
    /// Flag that controls if all the buttons are pressed
    /// </summary>
    bool m_AllButtonsPressed;
    bool m_AllTriggersPressed;
    /// <summary>
    /// Flag that sees if it is the first time the buttons are pressed
    /// </summary>
    bool m_FirstTimeButtonsPressed;
    bool m_FirstTimeTriggersPressed;

    // Values to calculate the rotation of the room
    Vector3 m_MovesDistanceVector;
    float currentDistance;
    float oldDistance;
    [SerializeField]
    float diffDist;

    [SerializeField]
    float difZ;
    float oldDifZ;
    [SerializeField]
    float superDifZ;
    [SerializeField]
    float initialSuperDifZ;
    [SerializeField]
    float difY;
    float oldDifY;
    [SerializeField]
    float superDifY;
    [SerializeField]
    float initialSuperDifY;
    [SerializeField]
    float difX;
    float oldDifX;
    [SerializeField]
    float superDifX;
    [SerializeField]
    float initialSuperDifX;

    // Diffs for each Y
    // Left
    float difLeftY;
    float oldDifLeftY;
    float frameDifLeftY;
    // Right
    float difRightY;
    float oldDifRightY;
    float frameDifRightY;
    // FrameDiffApproximation
    [SerializeField, Range(0f, 0.5f) ,Tooltip("Approximation value when comparing both Y values in each hand")]
    float m_FrameDiffApproximation;

    [SerializeField, Range(0f, 0.02f)]
    float m_DeadLimitRot;

    [SerializeField, Range(0f, 1000f)]
    float m_RotSensitivity;
    [SerializeField, Range(0f, 10f)]
    float m_ZAxisSensitivity;
    [SerializeField, Range(0f, 10f)]
    float m_YAxisSensitivity;
    [SerializeField, Range(0f, 15f)]
    float m_ZoomSensitivy;

    /// <summary>
    /// The transform that we will be filtering with the lowPass filter to remove jitter
    /// </summary>
    private Transform m_TransformFiltered;
    /// <summary>
    /// The raw target transfer (will contain a lot of jitter)
    /// </summary>
    private Transform m_TargetTransform;

    /// <summary>
    /// LowPass Filter factor. Value should be between 0.01f and 0.99f. Smaller value is more damping.
    /// </summary>
    [SerializeField, Range(0.01f, 1f), Tooltip("Value should be between 0.01f and 0.99f.Smaller value is more damping."), Header("Filtering Params")]
    float lowPassFactor = 0.8f;
    /// <summary>
    /// initialization of the lowPass filter done once
    /// </summary>
    bool init = true;

    #endregion

    #region UnityFunctions

    // This function is called when the object becomes enabled and active
    private void OnEnable()
    {
        // Sets the room as the target for the depth of field
        try
        {
            Toolbox.Instance.GameManager.MainCamera.gameObject.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField>()
    .focalTransform = this.gameObject.transform;
        }
        catch (System.Exception)
        {

            Debug.LogError("DepthofField returns null!");
        }

        // We set the initial pos of the player to the one of the token
        m_InitialPositionOfPlayer = m_InitialPosToken.position;

        // Sets the ball to the initial position in the room
        //m_PlayerInLevel.transform.localPosition = m_InitialPositionOfPlayer;
        SetPlayerAtInitialPos();

        // Sets rotation of the room to zero
        this.transform.rotation = Quaternion.identity;

        // Initializes the inital filtered transform
        m_TargetTransform = this.transform;
        m_TransformFiltered = m_TargetTransform;

        
    }

    // Use this for initialization
    void Start () {
        if (m_GameLogic == null)
        {
            m_GameLogic = (Toolbox.Instance.GameManager.GameLogicController as LD_LogicCtrl);
        }

        // Set buttonsPressed to the same lenght as the array of moves
        m_ButtonsPressed = new bool[m_GameLogic.Moves.Length];

        // Set triggersPressed to the same length as the array of moves
        m_TriggersPressed = new float[m_GameLogic.Moves.Length];

    }
	
	// Update is called once per frame
	void Update () {
        // We assume all the buttons are pressed
        m_AllButtonsPressed = true;
        m_AllTriggersPressed = true;

        // We go thorugh the buttons we want
        for (int i = 0; i < m_GameLogic.Moves.Length; i++)
        {
            if (Toolbox.Instance.GameManager.InputController.InputType == InputController.TypeOfInput.PSMove)
            {
                // Collect the values of the button to perform the rotation of the room
                m_ButtonsPressed[i] = m_GameLogic.Moves[i].IsCrossButtonDown;
                m_TriggersPressed[i] = m_GameLogic.Moves[i].TriggerValue;

                // If only button is not pressed, we set the flag to false
                if (!m_ButtonsPressed[i])
                {
                    m_AllButtonsPressed = false;
                }
                // The same for Triggers
                if (m_TriggersPressed[i] < 0.1f)
                {
                    m_AllTriggersPressed = false;
                }

                // TRIGGERS
                // ROTATION
                RotationPSMoveLogic(m_AllTriggersPressed);

                // BUTTONS
                // ZOOM
                ZoomPSMoveLogic(m_AllButtonsPressed);

            }
            // If the control through PSMove is deactivated...
            else
            {
                // Add support for usual input through mouse
                // If the left click is not down...
                if (!Input.GetMouseButton(0))
                {
                    // We map the triggers to the left click
                    m_AllTriggersPressed = false;
                }

                // If any of the up/down arrow jeys is pressed...
                if (!Input.GetKey(KeyCode.DownArrow) || !Input.GetKey(KeyCode.UpArrow))
                {
                    // We map the zooming with up/down arrows
                    m_AllButtonsPressed = false;
                }

                // Rotation
                RotationMouseLogic(m_AllTriggersPressed);

                // Zoom
                ZoomMouseLogic(m_AllButtonsPressed);
            }
        }
    }


    /// <summary>
    /// The logic of the rotation
    /// </summary>
    void RotationPSMoveLogic(bool flagToCheck) {
        // ROTATION
        // If all the values in the array are true...
        if (flagToCheck)
        {

            // Calculate differences in X, Y and Z every frame
            // If there are frames filtered...
            if (Toolbox.Instance.GameManager.InputController.NumFramesToFilter < 1)
            {
                // We use unfiltered values
                difZ = m_GameLogic.Moves[0].transform.position.z - m_GameLogic.Moves[1].transform.position.z;
                difY = m_GameLogic.Moves[0].transform.position.y - m_GameLogic.Moves[1].transform.position.y;
                difX = m_GameLogic.Moves[0].transform.position.x - m_GameLogic.Moves[1].transform.position.x;
            }
            // If there are frames filtered...
            else
            {
                // We use filtered values
                difZ = Toolbox.Instance.GameManager.InputController.MovesLerpedFilteredPos[0].z - Toolbox.Instance.GameManager.InputController.MovesLerpedFilteredPos[1].z;
                difY = Toolbox.Instance.GameManager.InputController.MovesLerpedFilteredPos[0].y - Toolbox.Instance.GameManager.InputController.MovesLerpedFilteredPos[1].y;
                difX = Toolbox.Instance.GameManager.InputController.MovesLerpedFilteredPos[0].x - Toolbox.Instance.GameManager.InputController.MovesLerpedFilteredPos[1].x;
            }


            // Calculate the differences in between frames for the rotation
            superDifZ = difZ - oldDifZ;
            superDifY = difY - oldDifY;
            superDifX = difX - oldDifX;

            // Deadzone checks to avoid jittery rotation
            if (Mathf.Abs(superDifZ) < m_DeadLimitRot)
            {
                superDifZ = 0f;
            }
            if (Mathf.Abs(superDifY) < m_DeadLimitRot)
            {
                superDifY = 0f;
            }
            if (Mathf.Abs(superDifX) < m_DeadLimitRot)
            {
                initialSuperDifX = 0f;
            }

            // Calculate the differences between each hand's Y
            // Gather Y values
            if (Toolbox.Instance.GameManager.InputController.NumFramesToFilter < 1)
            {
                difLeftY = m_GameLogic.Moves[0].transform.position.y;
                difRightY = m_GameLogic.Moves[1].transform.position.y;
            }
            else
            {
                difLeftY = Toolbox.Instance.GameManager.InputController.MovesLerpedFilteredPos[0].y;
                difRightY = Toolbox.Instance.GameManager.InputController.MovesLerpedFilteredPos[1].y;
            }

            // Diff them with the old ones
            frameDifLeftY = difLeftY - oldDifLeftY;
            frameDifRightY = difRightY - oldDifRightY;

            // Store current difs in the old ones
            oldDifLeftY = difLeftY;
            oldDifRightY = difRightY;

            if (m_FirstTimeTriggersPressed)
            {
                // Store the initial differences in positions
                initialSuperDifY = superDifY;
                initialSuperDifZ = superDifZ;
                initialSuperDifX = superDifX;

                // Store it as an absolute value
                initialSuperDifY = Mathf.Abs(initialSuperDifY);
                initialSuperDifZ = Mathf.Abs(initialSuperDifZ);
                initialSuperDifX = Mathf.Abs(initialSuperDifX);

                // initialization of the lowPass filter done once
                init = true;

                // Set the target transform to this transform
                m_TargetTransform = this.transform;

                this.m_TransformFiltered.rotation = LowPassFilterQuaternion(m_TransformFiltered.rotation, m_TargetTransform.rotation, lowPassFactor, init);
                init = false;
            }
            // Frame after the buttons have been pressed
            else
            {

                // Rotate left-right
                //this.transform.Rotate(Vector3.up, (superDifZ) * m_RotSensitivity * m_ZAxisSensitivity, Space.World); //This was the last one commented
                m_TargetTransform.Rotate(Vector3.up, (superDifZ) * m_RotSensitivity * m_ZAxisSensitivity, Space.World);
                //this.transform.Rotate(Vector3.up, (superDifY) * m_RotSensitivity, Space.World);
                /* This should be done by moving both PSMove forward? Or using the pitch value? */
                //this.transform.Rotate(Vector3.up, (superDifX) * m_RotSensitivity, Space.World);

                // Rotate down-up
                //this.transform.Rotate(Vector3.left, (superDifY) * m_RotSensitivity * 0.5f, Space.World);
                //this.transform.Rotate(Vector3.left, (superDifZ) * m_RotSensitivity, Space.World);
                // If both hands have a similar difference in Y (they are moving the same way)
                if (ReusableMethods.Floats.IsApproximately(frameDifLeftY, frameDifRightY, m_FrameDiffApproximation))
                {
                    // Rotate down-up
                    //this.transform.Rotate(Vector3.left, (superDifY) * m_RotSensitivity * 0.5f, Space.World);
                    //this.transform.Rotate(Vector3.left, (superDifZ) * m_RotSensitivity, Space.World);
                    //this.transform.Rotate(Vector3.left, (frameDifLeftY) * m_RotSensitivity * -1f * m_YAxisSensitivity, Space.World); // this was the last one commented
                    m_TargetTransform.Rotate(Vector3.left, (frameDifLeftY) * m_RotSensitivity * -1f * m_YAxisSensitivity, Space.World);
                }

                // Rotate Yaw of cube
                //this.transform.Rotate(Vector3.forward, (superDifY) * m_RotSensitivity * -1f, Space.World);
                m_TargetTransform.Rotate(Vector3.forward, (superDifY) * m_RotSensitivity * -1f, Space.World);

                this.m_TransformFiltered.rotation = LowPassFilterQuaternion(m_TransformFiltered.rotation, m_TargetTransform.rotation, lowPassFactor, init);

                this.transform.rotation = m_TransformFiltered.rotation;
            }

            // Store this frame's values
            oldDifY = difY;
            oldDifZ = difZ;
            oldDifX = difX;

            m_FirstTimeTriggersPressed = false;
        }
        else
        {
            oldDifY = 0f;
            oldDifZ = 0f;
            oldDifX = 0f;
            initialSuperDifY = 0f;
            initialSuperDifZ = 0f;
            initialSuperDifX = 0f;
            m_FirstTimeTriggersPressed = true;

            oldDifLeftY = 0;
            oldDifRightY = 0;
        }


    }

    /// <summary>
    /// Logic of the rotation using mouse
    /// </summary>
    /// <param name="flagToCheck"></param>
    void RotationMouseLogic(bool flagToCheck) {
        // ROTATION
        // If all the values in the array are true...
        if (flagToCheck)
        {

            // Calculate differences in X, Y and Z every frame
            difZ = Toolbox.Instance.GameManager.InputController.ScreenPointerPos.z;
            difY = Toolbox.Instance.GameManager.InputController.ScreenPointerPos.y;
            difX = Toolbox.Instance.GameManager.InputController.ScreenPointerPos.x;

            // Calculate the differences in between frames for the rotation
            superDifZ = difZ - oldDifZ;
            superDifY = difY - oldDifY;
            superDifX = difX - oldDifX;

            //// Deadzone checks to avoid jittery rotation
            //if (Mathf.Abs(superDifZ) < m_DeadLimitRot)
            //{
            //    superDifZ = 0f;
            //}
            //if (Mathf.Abs(superDifY) < m_DeadLimitRot)
            //{
            //    superDifY = 0f;
            //}
            //if (Mathf.Abs(superDifX) < m_DeadLimitRot)
            //{
            //    initialSuperDifX = 0f;
            //}


            if (m_FirstTimeTriggersPressed)
            {
                // Store the initial differences in positions
                initialSuperDifY = superDifY;
                initialSuperDifZ = superDifZ;
                initialSuperDifX = superDifX;

                // Store it as an absolute value
                initialSuperDifY = Mathf.Abs(initialSuperDifY);
                initialSuperDifZ = Mathf.Abs(initialSuperDifZ);
                initialSuperDifX = Mathf.Abs(initialSuperDifX);
            }
            else
            {

                // Rotate left-right
                //this.transform.Rotate(Vector3.up, (superDifZ) * m_RotSensitivity * m_ZAxisSensitivity, Space.World);
                //this.transform.Rotate(Vector3.up, (superDifY) * m_RotSensitivity, Space.World);
                /* This should be done by moving both PSMove forward? Or using the pitch value? */
                this.transform.Rotate(Vector3.up, (superDifX) * m_RotSensitivity * Time.smoothDeltaTime * -0.05f, Space.World);

                // Rotate down-up
                this.transform.Rotate(Vector3.left, (superDifY) * m_RotSensitivity * Time.smoothDeltaTime * -0.05f, Space.World);
                //this.transform.Rotate(Vector3.left, (superDifZ) * m_RotSensitivity, Space.World);
                // If both hands have a similar difference in Y (they are moving the same way)
                //if (ReusableMethods.Floats.IsApproximately(frameDifLeftY, frameDifRightY, m_FrameDiffApproximation))
                //{
                //    // Rotate down-up
                //    //this.transform.Rotate(Vector3.left, (superDifY) * m_RotSensitivity * 0.5f, Space.World);
                //    //this.transform.Rotate(Vector3.left, (superDifZ) * m_RotSensitivity, Space.World);
                //    this.transform.Rotate(Vector3.left, (frameDifLeftY) * m_RotSensitivity * -1f * m_YAxisSensitivity, Space.World);
                //}   

            }

            // Store this frame's values
            oldDifY = difY;
            oldDifZ = difZ;
            oldDifX = difX;

            m_FirstTimeTriggersPressed = false;
        }
        else
        {
            oldDifY = 0f;
            oldDifZ = 0f;
            oldDifX = 0f;
            initialSuperDifY = 0f;
            initialSuperDifZ = 0f;
            initialSuperDifX = 0f;
            m_FirstTimeTriggersPressed = true;
        }

        // YAW ROTATION
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // Rotate Yaw of cube
            this.transform.Rotate(Vector3.forward, (0.5f) * m_RotSensitivity * Time.smoothDeltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            // Rotate Yaw of cube
            this.transform.Rotate(Vector3.forward, (-0.5f) * m_RotSensitivity * Time.smoothDeltaTime, Space.World);
        }

    }

    /// <summary>
    /// Logic of the zoom
    /// </summary>
    /// <param name="flagToCheck"></param>
    void ZoomPSMoveLogic(bool flagToCheck) {
        // If all the triggers are pressed
        if (flagToCheck)
        {

            // We get the distance vector between the 2 controllers
            m_MovesDistanceVector = m_GameLogic.Moves[0].transform.position - m_GameLogic.Moves[1].transform.position;
            currentDistance = m_MovesDistanceVector.magnitude;
            diffDist = currentDistance - oldDistance;

            // Deadzone for the diff distance
            if (Mathf.Abs(diffDist) < m_DeadLimitRot)
            {
                diffDist = 0f;
            }

            if (m_FirstTimeButtonsPressed)
            {

            }
            else
            {
                // Zoom
                float valueZ = transform.position.z;
                valueZ = valueZ * diffDist;
                Vector3 auxPos = transform.position;
                auxPos.z -= valueZ * m_ZoomSensitivy;
                auxPos.z = Mathf.Clamp(auxPos.z, Camera.main.transform.position.z + Toolbox.Instance.GameManager.InputController.CameraOffset - 0.5f, 3f);
                //transform.position = transform.position + (Vector3.forward * diffDist * m_Sensitivity);
                transform.position = Vector3.Lerp(transform.position, auxPos, m_ZoomSensitivy * Time.fixedDeltaTime);
                //transform.position = auxPos;
            }

            // Store this frame's values
            oldDistance = currentDistance;

            m_FirstTimeButtonsPressed = false;
        }
        else
        {
            oldDistance = 0f;
            diffDist = 0f;
            m_FirstTimeButtonsPressed = true;
        }

    }

    /// <summary>
    /// Logic of the zoom with the mouse input
    /// </summary>
    /// <param name="flagToCheck"></param>
    void ZoomMouseLogic(bool flagToCheck) {
        // Zoom
        float valueZ = 0f;

        // Key up gets the cube closer
        if (Input.GetKey(KeyCode.UpArrow))
        {
            valueZ = transform.position.z;
            valueZ = valueZ + 5f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            valueZ = transform.position.z;
            valueZ = valueZ - 5f;
        }

        Vector3 auxPos = transform.position;
        auxPos.z -= valueZ * m_ZoomSensitivy;
        auxPos.z = Mathf.Clamp(auxPos.z, Camera.main.transform.position.z + Toolbox.Instance.GameManager.InputController.CameraOffset - 0.5f, 3f);
        //transform.position = transform.position + (Vector3.forward * diffDist * m_Sensitivity);
        transform.position = Vector3.Lerp(transform.position, auxPos, m_ZoomSensitivy * Time.smoothDeltaTime * 0.1f);
        //transform.position = auxPos;


    }

    private Quaternion LowPassFilterQuaternion (Quaternion intermediatevalue, Quaternion targetValue, float factor, bool init)
    {
        // intermediateValue needs to be initialized at the first usage
        if (init)
        {
            intermediatevalue = targetValue;
        }

        intermediatevalue = Quaternion.Lerp(intermediatevalue, targetValue, factor);
        return intermediatevalue;
    }

    /// <summary>
    /// Sets the player at the inital pos according to the token
    /// </summary>
    public void SetPlayerAtInitialPos()
    {
        m_PlayerInLevel.transform.position = m_InitialPositionOfPlayer;
        //m_PlayerInLevel.GetComponent<GamePadInput>().ResetRotation();

        //// Play respawn sound
        //Toolbox.Instance.GameManager.AudioController.PlayEvent("Respawn", m_PlayerInLevel);
    }




    #endregion

}
